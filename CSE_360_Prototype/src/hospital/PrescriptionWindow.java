package hospital;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Component;

import javax.swing.JTextArea;

public class PrescriptionWindow {

	protected JFrame frame, providerGUI, viewPatientWindow;
	private Issue issue;
	private Patient patient;
	private int providerID;

	/**
	 * Create the application.
	 * @param anIssue 
	 * @param anPatient 
	 */
	public PrescriptionWindow(Issue anIssue, Patient anPatient, JFrame pGUI, JFrame vPW, int pID) {
		issue = anIssue;
		patient = anPatient;
		providerGUI = pGUI;
		viewPatientWindow = vPW;
		providerID = pID;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 748, 88);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JLabel issueLabel = new JLabel(getValues());
		JTextArea prescriptionPane = new JTextArea();
		prescriptionPane.setWrapStyleWord(true);
		prescriptionPane.setToolTipText("Enter your prescription here.  The contents of this JTextArea are sent directly as a String.");
		prescriptionPane.setLineWrap(true);
		prescriptionPane.setRows(3);
		prescriptionPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		prescriptionPane.setAlignmentY(Component.TOP_ALIGNMENT);
		JButton submitPrescriptionButton = new JButton("Submit Prescription");
		submitPrescriptionButton.setToolTipText("Submits the contents of the JTextField to be viewed by the Patient");
		frame.getContentPane().setLayout(new GridLayout(0, 3, 0, 0));
		frame.getContentPane().add(issueLabel);
		frame.getContentPane().add(prescriptionPane);
		frame.getContentPane().add(submitPrescriptionButton);
		frame.getRootPane().setDefaultButton(submitPrescriptionButton);
		submitPrescriptionButton.addActionListener(new SubmitPrescriptionActionListener(issue, prescriptionPane, submitPrescriptionButton));
	}
	
	private String getValues() {
		String values = "";
		values = issue.getBodyPart() + issue.getSymptom(0) + ", " + issue.getSymptom(1) + ", " + issue.getSymptom(2);
		return values;
	}
	
	private class SubmitPrescriptionActionListener implements ActionListener {
		JTextArea text = null;
		JButton submitPrescriptionButton;
		public SubmitPrescriptionActionListener(Issue issue, JTextArea prescriptionText, JButton button) {
			text = prescriptionText;
			submitPrescriptionButton = button;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(!text.getText().equals("")) {
				String prescription = text.getText();
				issue.setPrescription(prescription);
				ArrayList<Patient> patients = Patient.getCurrentPatients();
				int index = -1;
				for(int i = 0; i < patients.size(); ++i) {
					if(patient.getID() == patients.get(i).getID()) {
						index = i;
						patient.serializeObject(index);
						i = patients.size(); //terminates the loop naturally.
					}
				}
				submitPrescriptionButton.setEnabled(false);
				ProviderGUI pGUI = new ProviderGUI(providerID);
				viewPatientWindow.dispose();
				viewPatientWindow = null;
				providerGUI.dispose();
				providerGUI = null;
				frame.dispose();
				frame = null;
				pGUI.frame.setVisible(true);
				
			}
		}

	}
}
