package hospital;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.UIManager;

import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.border.Border;

public class ManageAccountGUI {

	protected JFrame frame, personalInformationFrame, passedFrame;
	private Patient matchedPatient;
	private Provider matchedProvider;
	private String[] userInformation;
	private Account matchedAccount;
	//private int ID;
	private PatientGUI pGW;
	private ProviderGUI prGW;
	private boolean firstNameChanged;
	private JTextField changeUsernameField, firstNameField, middleNameField,
	lastNameField, ssnField, addressField, phoneField;
	private JLabel outputLabel, errorFeedbackLabel;
	private JPasswordField changePasswordField;
	/**
	 * Create the application.
	 * @param i 
	 */
	
	public ManageAccountGUI(int id) {
		//ID = id;
		firstNameChanged = false;
		matchedAccount = getMatchedAccount(id - 1);
		loadManageAccount();
	}
	
	// overload constructor
	public ManageAccountGUI(int id, Patient patient, JFrame pass) {
		//ID = id;
		firstNameChanged = false;
		passedFrame = pass;
		matchedPatient = patient;
		matchedAccount = getMatchedAccount(id - 1);
		loadManageAccount();
	}
	
	public ManageAccountGUI(int id, Provider provider, JFrame pass) {
		//ID = id;
		firstNameChanged = false;
		passedFrame = pass;
		matchedProvider = provider;
		matchedAccount = getMatchedAccount(id - 1);
		loadManageAccount();
	}

	private void clearFrame() {//Placed at the top as it is the most frequently referenced method.
		frame.getContentPane().removeAll();
		frame.validate();
		//frame.getContentPane().invalidate();
		//frame.getContentPane().repaint();
	}
	
	private Account getMatchedAccount(int indexToFind) {
		Account account = null;
		ArrayList<Account> accounts = Account.getCurrentAccounts();
		account = accounts.get(indexToFind);
		accounts = null;
		return account;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void loadManageAccount() {
		if(frame == null)
			frame = new JFrame();
		frame.setTitle("Manage Account Options");
		frame.setBounds(passedFrame.getX() + passedFrame.getWidth() + 5, passedFrame.getY(), 357, 250);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel manageAccountPanel = new JPanel();
		frame.getContentPane().add(manageAccountPanel);
		manageAccountPanel.setLayout(new GridLayout(4, 0, 0, 0));
		
		JButton viewPersonalInformationButton = new JButton("View Personal Information");
		viewPersonalInformationButton.addActionListener(new ViewPersonalInformationActionListener());
		manageAccountPanel.add(viewPersonalInformationButton);
		
		JButton changePersonalInformationButton = new JButton("Change Personal Information");
		changePersonalInformationButton.addActionListener(new ChangePersonalInformationActionListener());
		manageAccountPanel.add(changePersonalInformationButton);
		
		JButton changeUsernamePanel = new JButton("Change Username");
		changeUsernamePanel.addActionListener(new ChangeUsernamePanelActionListener());
		manageAccountPanel.add(changeUsernamePanel);
		
		JButton changePasswordPanel = new JButton("Change Password");
		changePasswordPanel.addActionListener(new ChangePasswordPanelActionListener());
		manageAccountPanel.add(changePasswordPanel);
		
		frame.validate();
	}
	
	private void loadChangeUsername() {
		frame.setTitle("Change Username");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(4, 1, 0, 0));
		
		JButton cancelPassword = new JButton("Back");
		cancelPassword.addActionListener(new CancelButtonActionListener());
		frame.getContentPane().add(cancelPassword);
		
		JPanel changeUsernamePanel = new JPanel();
		frame.getContentPane().add(changeUsernamePanel);
		changeUsernamePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel changeUsernameLabel = new JLabel("Enter your new username: ");
		changeUsernamePanel.add(changeUsernameLabel);
		
		changeUsernameField = new JTextField();
		changeUsernamePanel.add(changeUsernameField);
		changeUsernameField.setColumns(10);
		
		outputLabel = new JLabel();
		outputLabel.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(outputLabel);
		
		JButton changeUsernameButton = new JButton("Submit Username");
		changeUsernameButton.addActionListener(new ChangeUsernameButtonActionListener());
		frame.getContentPane().add(changeUsernameButton);
		
		frame.validate();
	}
	
	private void loadChangePassword() {
		frame.setTitle("Change Password");
		//frame.setBounds(100, 100, 352, 143);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(4, 1, 0, 0));
		
		JButton cancelPassword = new JButton("Back");
		cancelPassword.addActionListener(new CancelButtonActionListener());
		frame.getContentPane().add(cancelPassword);
		
		JPanel changePasswordPanel = new JPanel();
		frame.getContentPane().add(changePasswordPanel);
		changePasswordPanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel changeUsernameLabel = new JLabel("Enter your new password: ");
		changePasswordPanel.add(changeUsernameLabel);
		
		changePasswordField = new JPasswordField();
		changePasswordPanel.add(changePasswordField);
		changePasswordField.setColumns(10);
		
		if(outputLabel != null)
			outputLabel = null;
		outputLabel = new JLabel();
		outputLabel.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(outputLabel);
		
		JButton changePasswordButton = new JButton("Submit Password");
		changePasswordButton.addActionListener(new ChangePasswordButtonActionListener());
		frame.getContentPane().add(changePasswordButton);
		
		frame.validate();
	}

	private class ChangeUsernamePanelActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			changeUsernamePressed();
		}
		
		private void changeUsernamePressed() {
			clearFrame();
			loadChangeUsername();
		}
	}
	
	private class ChangePasswordPanelActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clearFrame();
			loadChangePassword();
		}
	}
	
	private class ChangePersonalInformationActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clearFrame();
			loaduserinformationGUI();
		}
	}
	
	private void loadViewPersonalInformation()
	{
		frame.setTitle("Personal Information");
		frame.setBounds(passedFrame.getX() + passedFrame.getWidth() + 5, passedFrame.getY(), 357, 250);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		if(matchedAccount.getAccountType() == 0)
			frame.getContentPane().setLayout(new GridLayout(8, 1, 0, 0));
		else
			frame.getContentPane().setLayout(new GridLayout(7, 1, 0, 0));
		
		JLabel firstNameLabel, middleNameLabel, lastNameLabel,
		SSNLabel, addressLabel, phoneLabel;
		String[] names;
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(new CancelButtonActionListener());
		
		JLabel primaryProviderLabel = new JLabel();
		String primaryPrvName = "";
		if(matchedAccount.getAccountType() == 0) {
			names = matchedPatient.getName().split(",");
			ArrayList<Provider> providerlist = Provider.getCurrentProviders();
			for(int i = 0; i < providerlist.size(); ++i)
				for(Provider prv : providerlist)
					if(prv.getID() == matchedPatient.getPrimaryProviderID())
						primaryPrvName = prv.getName();
			firstNameLabel = new JLabel("<html><font-family:timesnewroman><font size=4>First name: <font color='blue'>" + names[0] + "</font></font></font></html>");
			middleNameLabel = new JLabel("<html><font-family:timesnewroman><font size=4>Middle name: <font color='blue'>" + names[1] + "</font></font></font></html>");
			lastNameLabel = new JLabel("<html><font-family:timesnewroman><font size=4>Last name: <font color='blue'>" + names[2] + "</font></font></font></html>");
			SSNLabel = new JLabel("<html><font-family:timesnewroman><font size=4>SSN: <font color='blue'>" + matchedPatient.getSSN() + "</font></font></font></html>");
			addressLabel = new JLabel("<html><font-family:timesnewroman><font size=4>Address: <font color='blue'>" + matchedPatient.getAddress() + "</font></font></font></html>");
			phoneLabel = new JLabel("<html><font-family:timesnewroman><font size=4>Phone number: <font color='blue'>" + matchedPatient.getPhone() + "</font></font></font></html>");
			primaryProviderLabel.setText("<html><font-family:timesnewroman><font size=4>Primary Provider: <font color='blue'>" + primaryPrvName + "</font></font></font></html>");
		}
		else
		{
			names = matchedProvider.getName().split(",");
			firstNameLabel = new JLabel("<html><font-family:timesnewroman><font size=4>First name: <font color='blue'>" + names[0] + "</font></font></font></html>");
			middleNameLabel = new JLabel("<html><font-family:timesnewroman><font size=4>Middle name: <font color='blue'>" + names[1] + "</font></font></font></html>");
			lastNameLabel = new JLabel("<html><font-family:timesnewroman><font size=4>Last name: <font color='blue'>" + names[2] + "</font></font></font></html>");
			SSNLabel = new JLabel("<html><font-family:timesnewroman><font size=4>SSN: <font color='blue'>" + matchedProvider.getSSN() + "</font></font></font></html>");
			addressLabel = new JLabel("<html><font-family:timesnewroman><font size=4>Address: <font color='blue'>" + matchedProvider.getAddress() + "</font></font></font></html>");
			phoneLabel = new JLabel("<html><font-family:timesnewroman><font size=4>Phone number: <font color='blue'>" + matchedProvider.getPhone() + "</font></font></font></html>");
		}
		
		frame.add(backButton);
		frame.add(firstNameLabel);
		frame.add(middleNameLabel);
		frame.add(lastNameLabel);
		frame.add(SSNLabel);
		frame.add(addressLabel);
		frame.add(phoneLabel);
		
		if(matchedAccount.getAccountType() == 0)
			frame.add(primaryProviderLabel);
		
		frame.validate();
	}
	
	private void loaduserinformationGUI()
	{
		frame.setTitle("Change Personal Information");
		frame.setBounds(passedFrame.getX() + passedFrame.getWidth() + 5, passedFrame.getY(), 600, 500);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(4, 1, 0, 10));
		
		JPanel exitPersonalInformationPanel = new JPanel();
		frame.getContentPane().add(exitPersonalInformationPanel);
		exitPersonalInformationPanel.setLayout(new GridLayout(1, 1, 50, 0));
		
		JButton cancelButton = new JButton("Back");
		cancelButton.addActionListener(new CancelButtonActionListener());
		exitPersonalInformationPanel.add(cancelButton);
		
		JPanel enterNamePanel = new JPanel();
		frame.getContentPane().add(enterNamePanel);
		enterNamePanel.setLayout(new GridLayout(0, 3, 0, 0));
		
		JPanel firstNamePanel = new JPanel();
		enterNamePanel.add(firstNamePanel);
		firstNamePanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		JLabel firstNameLabel = new JLabel("Enter First Name");
		firstNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		firstNamePanel.add(firstNameLabel);
		
		firstNameField = new JTextField();
		firstNameField.setHorizontalAlignment(SwingConstants.CENTER);
		firstNamePanel.add(firstNameField);
		firstNameField.setColumns(10);
		
		JPanel middleNamePanel = new JPanel();
		enterNamePanel.add(middleNamePanel);
		middleNamePanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		JLabel middleNameLabel = new JLabel("Enter Middle Name");
		middleNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		middleNamePanel.add(middleNameLabel);
		
		middleNameField = new JTextField();
		middleNameField.setHorizontalAlignment(SwingConstants.CENTER);
		middleNamePanel.add(middleNameField);
		middleNameField.setColumns(10);
		
		JPanel lastNamePanel = new JPanel();
		enterNamePanel.add(lastNamePanel);
		lastNamePanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		JLabel lastNameLabel = new JLabel("Enter Last Name");
		lastNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lastNamePanel.add(lastNameLabel);
		
		lastNameField = new JTextField();
		lastNameField.setHorizontalAlignment(SwingConstants.CENTER);
		lastNamePanel.add(lastNameField);
		lastNameField.setColumns(10);
		
		JPanel personalInformationPanel = new JPanel();
		frame.getContentPane().add(personalInformationPanel);
		personalInformationPanel.setLayout(new GridLayout(0, 3, 0, 0));
		
		JPanel ssnPanel = new JPanel();
		personalInformationPanel.add(ssnPanel);
		ssnPanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		JLabel ssnLabel = new JLabel("Enter SSN");
		ssnLabel.setHorizontalAlignment(SwingConstants.CENTER);
		ssnPanel.add(ssnLabel);
		
		ssnField = new JTextField();
		ssnField.setHorizontalAlignment(SwingConstants.CENTER);
		ssnPanel.add(ssnField);
		ssnField.setColumns(10);
		
		JPanel addressPanel = new JPanel();
		personalInformationPanel.add(addressPanel);
		addressPanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		JLabel addressLabel = new JLabel("Enter Address");
		addressLabel.setHorizontalAlignment(SwingConstants.CENTER);
		addressPanel.add(addressLabel);
		
		addressField = new JTextField();
		addressField.setHorizontalAlignment(SwingConstants.CENTER);
		addressPanel.add(addressField);
		addressField.setColumns(10);
		
		JPanel phonePanel = new JPanel();
		personalInformationPanel.add(phonePanel);
		phonePanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		JLabel phoneLabel = new JLabel("Enter Phone Number");
		phoneLabel.setHorizontalAlignment(SwingConstants.CENTER);
		phonePanel.add(phoneLabel);
		
		phoneField = new JTextField();
		phoneField.setHorizontalAlignment(SwingConstants.CENTER);
		phonePanel.add(phoneField);
		phoneField.setColumns(10);
		
		//errorFeedbackPanel = new JPanel();
		//frame.getContentPane().add(errorFeedbackPanel);
		//errorFeedbackPanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		JPanel submitPanel = new JPanel();
		frame.getContentPane().add(submitPanel);
		submitPanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		errorFeedbackLabel = new JLabel();
		errorFeedbackLabel.setHorizontalAlignment(SwingConstants.CENTER);
		submitPanel.add(errorFeedbackLabel);
		
		JButton submitUserButton = new JButton("Submit User Information");
		submitUserButton.addActionListener(new SubmitUserButtonActionListener());
		submitPanel.add(submitUserButton);
		frame.getRootPane().setDefaultButton(submitUserButton);
		
		frame.validate();
		//frame.getContentPane().invalidate();
		//frame.getContentPane().repaint();
	}
	
	private class ViewPersonalInformationActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clearFrame();
			loadViewPersonalInformation();
		}
	}
	
	private class CancelButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clearFrame();
			loadManageAccount();
		}
	}
	
	private class SubmitUserButtonActionListener implements ActionListener {
		String errorOutput = "";
		Border defaultBorder = UIManager.getBorder("TextField.border");
		public void actionPerformed(ActionEvent arg0) {
			boolean acceptInformation = checkForValidFields();
			if(acceptInformation) {
				userInformation = new String[4];
				userInformation[0] = firstNameField.getText() + ", " + middleNameField.getText() + 
						", " + lastNameField.getText();
				userInformation[1] = ssnField.getText();
				userInformation[2] = addressField.getText();
				userInformation[3] = phoneField.getText();
				serializeChanges();
				clearFrame();
				if(firstNameChanged)
				{
					if(matchedAccount.getAccountType() == 0)
					{
						if(passedFrame != null)
							passedFrame.dispose();
						if(pGW != null)
							pGW.frame.dispose();
						pGW = new PatientGUI(matchedPatient.getID());
						pGW.frame.setVisible(true);
					}
					else
					{
						if(passedFrame != null)
							passedFrame.dispose();
						if(prGW != null)
							prGW.frame.dispose();
						prGW = new ProviderGUI(matchedProvider.getID());
						prGW.frame.setVisible(true);
					}
				}
				loadManageAccount();
			}
			else {
				errorFeedbackLabel.setText("<html>" + errorOutput + "</html>");
			}
			errorOutput = ""; // reset previously stored error message
			//loadMainLogin();
		}
		
		private void serializeChanges()
		{
			int accountType = matchedAccount.getAccountType();
			String[] newNames = userInformation[0].split(",");
			String[] currNames;
			if(accountType == 0)
				currNames = matchedPatient.getName().split(",");
			else
				currNames = matchedProvider.getName().split(",");
			String fullName;
			for(int i = 0; i < userInformation.length; ++i)
			{
				if(!userInformation[i].equals(""))
				{
					if(accountType == 0)
					{
						switch(i)
						{
							case 0:
								// if any of the name fields were unchanged, then
								// the new names for those fields will keep the old values
								if(newNames[0].length() < 1)
									newNames[0] = currNames[0];
								else
									firstNameChanged = true;
								if(newNames[1].length() < 1 || newNames[1].equals(" "))
									newNames[1] = currNames[1];
								if(newNames[2].length() < 1 || newNames[2].equals(" "))
									newNames[2] = currNames[2];
								
								fullName = newNames[0] + ", " + newNames[1] + ", " + newNames[2];
								matchedPatient.setName(fullName);
								break;
							case 1:
								matchedPatient.setSSN(userInformation[i]);
								break;
							case 2:
								matchedPatient.setAddress(userInformation[i]);
								break;
							case 3:
								matchedPatient.setPhone(userInformation[i]);
								break;
							default:
								break;
						}
					}
					else
					{
						switch(i)
						{
							case 0:
								if(newNames[0].length() < 1)
									newNames[0] = currNames[0];
								else
									firstNameChanged = true;
								if(newNames[1].length() < 1 || newNames[1].equals(" "))
									newNames[1] = currNames[1];
								if(newNames[2].length() < 1 || newNames[2].equals(" "))
									newNames[2] = currNames[2];
								
								fullName = newNames[0] + ", " + newNames[1] + ", " + newNames[2];
								matchedProvider.setName(fullName);
								break;
							case 1:
								matchedProvider.setSSN(userInformation[i]);
								break;
							case 2:
								matchedProvider.setAddress(userInformation[i]);
								break;
							case 3:
								matchedProvider.setPhone(userInformation[i]);
								break;
							default:
								break;
						}
					}
				}
			}
			
			if(accountType == 0)
			{
				ArrayList<Patient> patients = Patient.getCurrentPatients();
				for(int i = 0; i < patients.size(); ++i)
					if(patients.get(i).getID() == matchedPatient.getID())
						matchedPatient.serializeObject(i);
			}
			else if(accountType == 1)
			{
				ArrayList<Provider> providers = Provider.getCurrentProviders();
				for(int i = 0; i < providers.size(); ++i)
					if(providers.get(i).getID() == matchedProvider.getID())
						matchedProvider.serializeObject(i);
			}
		}
		
		private boolean isNumber(String toCheck) {
			for(int i = 0; i < toCheck.length(); ++i)
			{
				if(!Character.isDigit(toCheck.charAt(i)))
					return false;
			}
			
			return true;
		}
		
		private boolean checkForValidFields() {
			boolean accept = true;
			String appendRed = "", appendBlue = "", appendOrange = "";
			JTextField[] fields = new JTextField[] {firstNameField, middleNameField, lastNameField, ssnField, addressField, phoneField };
			CharSequence[] blackListedCharacters = new CharSequence[] {"/", "\\", " ", "\'", ":", "\""};
			CharSequence[] blackListedAddressChars = new CharSequence[] {"/", "\\", "\'", ":", "\""};
			CharSequence[] blackList;
			boolean[] validCharacters = new boolean[] {true, true, true, true, true, true};
			
			// check if all the fields are empty; do not let the user submit if they are
			if(areFieldsEmpty(fields))
			{
				for(int i = 0; i < validCharacters.length; ++i)
						fields[i].setBorder(defaultBorder);
				errorOutput = "You must enter information before you can submit.";
				return false;
			}
			else {
				for(int i = 0; i < fields.length; ++i) {
					String information = fields[i].getText();
					
					if(i == 4)
						blackList = blackListedAddressChars;
					else
						blackList = blackListedCharacters;
					
					if(i != 3 && i != 5)
					for(CharSequence c : blackList)
						if(information.contains(c)) {
							fields[i].setBorder(BorderFactory.createLineBorder(Color.RED, 1));
							appendRed = "<p>Boxes with a <font color='red'>red border</font> contain invalid characters.</p>";
							accept = false;
							validCharacters[i] = false;
							break;
						}
					
					if(i == 3 && information.length() > 0) {
						int ssnLength = fields[i].getText().length();
						if(ssnLength != 9 || !isNumber(fields[i].getText())) {
							fields[i].setBorder(BorderFactory.createLineBorder(Color.BLUE, 1));
							appendBlue = "<p>Your <font color='blue'>SSN</font> should contain 9 numbers.</p>";
							accept = false;
							validCharacters[i] = false;
						}
					}
					
					if (i == 5 && information.length() > 0){
						int phoneLength = fields[i].getText().length();
						if (phoneLength != 10 || !isNumber(fields[i].getText())){
							fields[i].setBorder(BorderFactory.createLineBorder(Color.ORANGE, 1));
							appendOrange = "<p>Your <font color='orange'>Phone Number</font> should contain 10 numbers.</p>";
							accept = false;
							validCharacters[i] = false;
						}
					}
				}
				
				// set the textfield borders, for which error-checking went smoothly, back to default
				for(int i = 0; i < validCharacters.length; ++i)
					if(validCharacters[i])
						fields[i].setBorder(defaultBorder);
				
				// append colored error labels to final error output message
				// if no error for a given color is unused, then the corresponding label is empty and is appended as an empty string
				errorOutput += appendBlue + appendRed + appendOrange;
				
				return accept;
			}
		}
	}
	
	private boolean areFieldsEmpty(JTextField[] fields)
	{
		for(JTextField field : fields)
		{
			if(field.getText().length() > 0)
				return false;
		}
		
		return true;
	}
	
	private class ChangeUsernameButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String username = changeUsernameField.getText();
			boolean unique = false;
			boolean blacklisted = false;
			CharSequence[] blackListedCharacters = new CharSequence[] {"/", "\\", " ", "\'", ":", "\""};
			
			if(!username.equals("")) {
				unique = checkUniqueUsername(username);
			}
			else
				outputLabel.setText("You must submit a username.");
			
			// check if the username contains any of the blacklisted characters
			for(CharSequence c: blackListedCharacters)
			{
				if(username.contains(c))
				{
					blacklisted = true;
					outputLabel.setText("<html>That username contains <font color='red'>invalid characters</font>.</html>");
					break;
				}
			}
			if(!blacklisted)
			{
				// check if the username is unique
				if(unique) {
					matchedAccount.setUsername(username);
					matchedAccount.serializeObject(matchedAccount.getUserID() - 1);
					outputLabel.setText("Your username is now: " + username);
					matchedAccount.output();
				}
				// check if username exists and it is not empty
				if(!unique && !username.equals(""))
					outputLabel.setText("That username already exists.");
			}
		}
		
		private boolean checkUniqueUsername(String username) {
			boolean unique = false;
			
			ArrayList<Account> accounts = Account.getCurrentAccounts();
			for(Account account : accounts) {
				if(account.getUsername().equals(username))
					return unique = false;
				else
					unique = true;
			}
			
			return unique;
		}
	}
	
	private class ChangePasswordButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			changePasswordPressed();
		}
		
		private void changePasswordPressed() {
			char[] password = changePasswordField.getPassword();
			
			// check if the entered password sequence is the same as the current password sequence
			if(password.length < 1)
				outputLabel.setText("You must submit a password.");
			else if(!Arrays.equals(matchedAccount.getPassword(), password))
			{
				matchedAccount.setPassword(password);
				matchedAccount.serializeObject(matchedAccount.getUserID() - 1);
				outputLabel.setText("Your password has been changed.");
			}
			else
				outputLabel.setText("That is already your current password.");
		}
	}
}
