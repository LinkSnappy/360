package hospital;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ViewPatientWindow {

	protected JFrame frame, providerGUIWindow, viewPatientWindow;
	protected PrescriptionWindow pw;
	protected Patient matchedPatient;
	protected int providerID;
	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewPatientWindow window = new ViewPatientWindow(null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the application.
	 * @param patient 
	 */
	public ViewPatientWindow(int IDToMatch, int toProviderID, JFrame pGW, JFrame vPW) {
		providerID = toProviderID;
		providerGUIWindow = pGW;
		viewPatientWindow = vPW;
		System.out.println("ID TO MATCH: " + IDToMatch);
		matchedPatient = getMatchedPatient(IDToMatch);
		loadViewPatientWindow(matchedPatient);
		//random comment.
	}
	
	private Patient getMatchedPatient(int IDToMatch) {
		ArrayList<Patient> patients = Patient.getCurrentPatients();
		for(Patient patient : patients) {
			if(patient.getID() == IDToMatch)
				return matchedPatient = patient;
		}
		return null;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void loadViewPatientWindow(Patient patient) {
		if(frame == null)
			frame = new JFrame();
		frame.setTitle(patient.getName() + " patient issues");
		frame.setBounds(100, 100, 750, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		
		JScrollPane patientIssueScrollPane = new JScrollPane();
		JPanel scrollPanelContents = fillPatientIssueScrollPane(patient);
		patientIssueScrollPane.setViewportView(scrollPanelContents);
		frame.getContentPane().add(patientIssueScrollPane);
	}
	
	private JPanel fillPatientIssueScrollPane(Patient patient) {
		Patient sortedPatient = new Patient(new String[] { patient.getName(), patient.getSSN(), patient.getAddress(), patient.getPhone() }, patient.getID(), patient.getPrimaryProviderID() );
		for(Issue issue : patient.issues) {
			Issue newIssue = new Issue(issue.getBodyPart(), issue.getSymptom(), issue.getIssueNumber());
			sortedPatient.addIssue(newIssue);
			newIssue.setPrescription(issue.getPrescription());
			newIssue.setStatus(issue.getStatus());
		}
		sortedPatient.issues = patient.InsertionSortIssue(sortedPatient.issues);
		int ID = matchedPatient.getID();
		
		JPanel panel = new JPanel();
		int rows;
		if(patient.issues != null)
			rows = patient.issues.size();
		else {
			rows = 1;
		}
		panel.setLayout(new GridLayout(rows, 6, 20, 30));
		int height = 0;
		if(patient.issues != null)
			height = patient.issues.size();
		else
			height = 1;
		panel.setPreferredSize(new Dimension(100, height * 60));
		if(patient.issues != null)
			for(int i = 0; i < 2; ++i)
				if(i == 0) {
					for(Issue issue : sortedPatient.issues) {
						if(issue.getStatus()){
							JLabel bodyPart = new JLabel("Type: " + issue.getBodyPart());
							JLabel degree = new JLabel("Values: " + issue.getSymptom(0) + ", " + issue.getSymptom(1) + ", " +
															issue.getSymptom(2));
							JLabel statusLabel = new JLabel("Status: " + getStatusText(issue.getStatus()));
							JLabel prescriptionLabel = new JLabel("Prescription: " + issue.getPrescription());
							JButton toggleStatusButton = new JButton("Open/Close");
							toggleStatusButton.addActionListener(new ToggleStatusButtonActionListener(patient, issue.getIssueNumber()));
							JButton prescriptionButton = new JButton("Write Prescription");
							prescriptionButton.addActionListener(new PrescriptionButtonActionListener( patient, issue.getIssueNumber()));
							
							panel.add(bodyPart);
							panel.add(degree);
							panel.add(statusLabel);
							panel.add(prescriptionLabel);
							panel.add(toggleStatusButton);
							panel.add(prescriptionButton);
						}
					}
				} else {
					for(Issue issue : sortedPatient.issues) {
						if(!issue.getStatus()){
							JLabel bodyPart = new JLabel("Type: " + issue.getBodyPart());
							JLabel degree = new JLabel("Values: " + issue.getSymptom(0) + ", " + issue.getSymptom(1) + ", " +
															issue.getSymptom(2));
							JLabel statusLabel = new JLabel("Status: " + getStatusText(issue.getStatus()));
							JLabel prescriptionLabel = new JLabel("Prescription: " + issue.getPrescription());
							JButton toggleStatusButton = new JButton("Open/Close");
							toggleStatusButton.addActionListener(new ToggleStatusButtonActionListener(patient, issue.getIssueNumber()));
							JButton prescriptionButton = new JButton("Write Prescription");
							prescriptionButton.addActionListener(new PrescriptionButtonActionListener(patient, issue.getIssueNumber()));
							
							panel.add(bodyPart);
							panel.add(degree);
							panel.add(statusLabel);
							panel.add(prescriptionLabel);
							panel.add(toggleStatusButton);
							panel.add(prescriptionButton);
						}
					}
				}
		return panel;
	}
	
	private String getStatusText(boolean status) {
		if(status)
			return "open";
		else 
			return "closed";
	}
	
	private class ToggleStatusButtonActionListener implements ActionListener {
		Patient thePatient;
		int theIssueNumber;
		Issue theIssue = null;
		
		public ToggleStatusButtonActionListener(Patient patient, int issueNumber) {
			thePatient = patient;
			theIssueNumber  = issueNumber;
			for(Issue issue : thePatient.issues) {
				if(issue.getIssueNumber() == theIssueNumber) {
					theIssue = issue;
				}
			}
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			theIssue.toggleStatus();
			ArrayList<Patient> patients = Patient.getCurrentPatients();
			for(int i = 0; i < patients.size(); ++i) {
				if(patients.get(i).getID() == thePatient.getID()) {
					thePatient.serializeObject(i);
					i = patients.size();//terminates the loop naturally without break;
				}
			}
			
			if(providerGUIWindow != null)
				providerGUIWindow.dispose();
			ProviderGUI pGW = new ProviderGUI(providerID);
			if(viewPatientWindow != null)
				viewPatientWindow.dispose();
			if(frame != null)
				frame.dispose();
			pGW.frame.setVisible(true);
			
		}
		
	}
	private class PrescriptionButtonActionListener implements ActionListener {
		Issue theIssue = null;
		Patient thePatient = null;
		int theIssueNumber = -1;
		
		public PrescriptionButtonActionListener(Patient patient, int issueNumber) {
			thePatient = patient;
			theIssueNumber = issueNumber;
			for(Issue issue : patient.issues) {
				if(issue.getIssueNumber() == theIssueNumber) {
					theIssue = issue;
				}
			}
			//ProviderGUI pGW = new ProviderGUI(providerID);
			//if(providerGUIWindow != null)
			//	providerGUIWindow.dispose();
			
			if(viewPatientWindow != null)
				viewPatientWindow.dispose();
			//if(frame != null)
				//frame.dispose();
			
		//	pGW.frame.setVisible(true);
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			pw = new PrescriptionWindow(theIssue, thePatient, providerGUIWindow, frame, providerID);
			pw.frame.setVisible(true);
		}
		
	}

}
