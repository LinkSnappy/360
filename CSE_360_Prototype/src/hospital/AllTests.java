package hospital;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase {

	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(PatientTest.class);
		suite.addTestSuite(AccountTest.class);
		suite.addTestSuite(IssueTest.class);
		suite.addTestSuite(ProviderTest.class);
		//$JUnit-END$
		return suite;
	}
}
