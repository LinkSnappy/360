package hospital;

import java.util.Calendar;
import java.util.Comparator;

public class Issue implements java.io.Serializable, Comparator<Issue>{
	private String bodyPart;
	private String prescription;
	private int[] date;
	private int[] symptom;
	private int urgency;
	private int issueNumber;
	private boolean status; //true == open.  false == closed.
	private static final long serialVersionUID = 1L;
	
	public Issue(String toBodyPart, int[] toSymptom, int toIssueNumber) {
		bodyPart = toBodyPart;
		symptom = new int[3];
		prescription = new String("");
		status = true;
		
		for(int i = 0; i < symptom.length; ++i) {
			symptom[i] = toSymptom[i];
		}
		date = assignDate();
		issueNumber = toIssueNumber;
		urgency = assignUrgency();
	}
	
	private int[] assignDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.getTime();//fills the calendar object with data.
		int[] toDate = new int[5];
		toDate[0] = calendar.get(Calendar.YEAR);
		toDate[1] = calendar.get(Calendar.MONTH) + 1;
		toDate[2] = calendar.get(Calendar.DAY_OF_MONTH);
		toDate[3] = calendar.get(Calendar.DAY_OF_WEEK);
		toDate[4] = calendar.get(Calendar.HOUR_OF_DAY);
		return toDate;
	}
	
	private int assignUrgency() {
		int sum = 0;
		for(int i : symptom) {
			sum += i;
		}
		return (sum/symptom.length);
	}
	public int getSymptom(int index) {
		return symptom[index];
	}
	
	public int[] getSymptom() {
		return symptom;
	}
	public String getBodyPart() {
		return bodyPart;
	}
	public int getYear() {
		return date[0];
	}
	
	public int getMonth() {
		return date[1];
	}
	
	public int getDayOfMonth() {
		return date[2];
	}
	
	public int getDayOfWeek() {
		return date[3];
	}
	
	public int getHourOfDay() {
		return date[4];
	}
	
	public int getIssueNumber() {
		return issueNumber;
	}
	
	/**
	 * @return the status
	 */
	public boolean getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean toStatus) {
		this.status = toStatus;
	}
	
	public void toggleStatus() {
		this.status = !this.status;
	}
	
	public void setPrescription(String toPrescription) {
		prescription = toPrescription;
	}
	
	public String getPrescription() {
		return prescription;
	}
	
	public int getUrgency() {
		return urgency;
	}
	
	
	public void output() {
		System.out.println("Year = " + this.getYear());
		System.out.println("Month = " + this.getMonth());
		System.out.println("Day of month = " + this.getDayOfMonth());
		System.out.println("Day of week = " + this.getDayOfWeek());
	}


	@Override
	public int compare(Issue o1, Issue o2) {
		if (o1.getUrgency() > o2.getUrgency())
			return 1;
		else
			return -1;
	}
}

