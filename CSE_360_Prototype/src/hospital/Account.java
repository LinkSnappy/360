package hospital;

import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@SuppressWarnings("rawtypes")
public class Account implements Databaser, java.io.Serializable {
	private String username;
	private char[] password;
	private int userID;
	private byte accountType;
	private static final long serialVersionUID = 1L;
	
	public Account(String toUsername, char[] toPassword, int toUserID) {
		this.setUsername(toUsername);
			this.setPassword(toPassword);
			this.setUserID(toUserID);
			accountType = (byte) 2;
	}
	
	@Override
	public boolean serializeObject(int indexToRemove) {
		// TODO Auto-generated method stub
		boolean success = false;
		Account account = this;
		List<Account> accounts = null;
		
		accounts = deserializeObjects();
		if(indexToRemove != -1 && accounts != null){
			try {
				accounts.remove(indexToRemove);
			} catch(IndexOutOfBoundsException ex) {
				outputError(new File("error.txt"));
			}
		}
		if(accounts == null)
			accounts = new ArrayList<Account>(); //instantiates a new ArrayList if deserializeObject returns null (will only happen if the target file is file not found or empty.
		accounts.add(account);
		if(accounts.size() > 1)
			Collections.sort(accounts, new AccountComparator()); // uses 
		try
	      {
	         FileOutputStream fileOut =
	         new FileOutputStream("account.ser");//boolean arguement is whether or not the outputwriter should append.
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(accounts);
	         out.close();
	         fileOut.close();
	         System.out.printf("Serialized data is saved in \"account.ser\"\n");
	         success = true;
	      }catch(IOException e)
	      {
	    	  success = false; //problem was caused and serialization 
	          e.printStackTrace();
	      }
		return success;
	}
	
	private void outputError(File file) {
		// TODO Auto-generated method stub
		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(file));
			dos.writeUTF("Index error occured after account.ser was read in.");
			System.out.println("Index error occured after account.ser was read in.");
			dos.close();
		}catch(IOException ex) {
			
		}
	}
	/**
	 * @return the username
	 */
	protected String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	protected void setUsername(String toUsername) {
		this.username = toUsername;
	}
	/**
	 * @return the password
	 */
	protected char[] getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	protected void setPassword(char[] toPassword) {
		this.password = toPassword;
	}
	/**
	 * @return the userID
	 */
	protected int getUserID() {
		return userID;
	}
	
	protected byte getAccountType() {
		return accountType;
	}
	
	protected void setUserType(byte toAccountType) {
		accountType = toAccountType;
	}
	
	protected static int generateUserID() {
		
		List<Account> accounts = getCurrentAccounts();
		int newID = 0;
		if(accounts != null)
			newID = accounts.get(accounts.size() - 1).getUserID();
		else
			newID = 0;
		return ++newID;
		
		/*List<Account> accounts = getCurrentAccounts();
		String currentMax = "";
		int nextID = 0;
		for(Account a : accounts) {
			if(a.getUserID().compareTo(currentMax) > 1) {
				currentMax = a.getUserID();
			}
		}
		char[] current = currentMax.toCharArray();
		char[] currentMinusOne = new char[current.length - 1];
		for(int i = 1; i < current.length - 1; ++i)
			currentMinusOne[i-1] = current[i];
		nextID = Integer.parseInt(currentMinusOne.toString());
		nextID++;
		currentMax = current[0] + Integer.toString(nextID);
		return currentMax;*/
		
	}
	/**
	 * @param userID the userID to set
	 */
	protected void setUserID(int userID) {
		this.userID = userID;
	}

	@Override
	public Patient getPatientData() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void output() {
		// TODO Auto-generated method stub
		System.out.println("\nusername = " + this.getUsername());
		System.out.println("password = " + this.getPassword().toString());
		System.out.println("userID = " + this.getUserID());
		System.out.println("userType = " + this.getAccountType());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Account> deserializeObjects() {
		ArrayList<Account> accounts = null;
		FileInputStream fileIn = null;
		try {
			fileIn = new FileInputStream("account.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			
			accounts = (ArrayList<Account>) in.readObject();
			in.close();
	        fileIn.close();
			
		}catch(EOFException  | FileNotFoundException e) {
	    	  return null;
		}catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return accounts;
		
	}
	
	@SuppressWarnings("unchecked")
	protected static ArrayList<Account> getCurrentAccounts() {
		ArrayList<Account> accounts = null;
		FileInputStream fileIn = null;
		try {
			fileIn = new FileInputStream("account.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			
			accounts = (ArrayList<Account>) in.readObject();
			in.close();
	        fileIn.close();
			
		}catch(EOFException  | FileNotFoundException e) {
	    	  return null;
		}catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
		}
		
		return accounts;
		
	}
	
	class AccountComparator implements Comparator<Account> {

		@Override
		public int compare(Account a1, Account a2) {
			// TODO Auto-generated method stub
			if(a1.getUserID() > a2.getUserID())
				return 1;
			else 
				return -1;
		}
		
	}
}
