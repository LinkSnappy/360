package hospital;

import junit.framework.TestCase;

public class AccountTest extends TestCase {

	public void testAccount() {
		char[] pass = new char[] {'p', 'a', 's', 's'};
		Account acc = new Account("user", pass, 1);
		
		assertEquals(true, (acc instanceof Account));

		char[] pass1 = new char[] {};
		Account acc1 = new Account("", pass1, 1);
		assertEquals(true, (acc1 instanceof Account));
	}

	public void testGetSetUsername() {
		char[] pass = new char[] {'p', 'a','s', 's'};
		Account acc = new Account("user", pass, 1);
		
		acc.setUsername("new name");
		String result = acc.getUsername();
		assertEquals("new name", result);

		acc.setUsername("");
		assertEquals("", acc.getUsername());
	}

	public void testGetSetPassword() {
		char[] pass = new char[] {'p', 'a', 's', 's'};
		Account acc = new Account("user", pass,  1);
		
		assertEquals(pass, acc.getPassword());

		char[] noPass = new char[1];
		acc.setPassword(noPass);
		assertEquals(noPass, acc.getPassword());
	}
}
