package hospital;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.GridLayout;

import javax.swing.JPasswordField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.awt.Component;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.UIManager;
import javax.swing.border.Border;

public class Login {

	
	private String username;
	private char[] password; //this will be a char[] in the final deliverable.
	private int ID;
	private String[] userInformation;
	
	protected JFrame frame;
	
	private JTextField usernameField, createUsernameField, firstNameField,
	middleNameField, lastNameField, ssnField, addressField, phoneField;
	
	private JPasswordField passwordField, createPasswordField;
	
	private JPanel usernamePanel, passwordPanel, createAccountPanel,
	createPasswordPanel, enterNamePanel, firstNamePanel, middleNamePanel,
	lastNamePanel, personalInformationPanel, loginInformationPanel,
	exitPersonalInformationPanel, ssnPanel, phonePanel, addressPanel, 
	verticalSpacePanel_1, submitPanel;
	
	private JButton loginButton, createAccountButton, 
	submitUsernamePasswordButton, createAccountBackButton, submitUserButton, 
	submitUserBackButton, cancelButton, chooseProviderButton;
	
	private JLabel createAccountLabel, createPasswordLabel, firstNameLabel, middleNameLabel, lastNameLabel, 
	ssnLabel, addressLabel, phoneLabel, errorFeedbackLabel, 
	errorFeedBackLabel, errorOutputLabel, chooseTypeErrorOutputLabel;
	
	private Component verticalStrut_3;
	
	JComboBox<String> comboBox;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login("");
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Constructor.
	 */
	public Login(String errorOutputDefaultText) {
		loadMainLogin(errorOutputDefaultText);
		//loadUserInformationGUI();
		//loadUsernamePasswordGUI();
	}
	
	/**
	 * used to clear the current frame of all it's contents so that other components may be added.
	 */
	private void clearFrame() {//Placed at the top as it is the most frequently referenced method.
		frame.getContentPane().removeAll();
		frame.validate();
		//frame.getContentPane().invalidate();
		//frame.getContentPane().repaint();
	}
	/**
	 * The first part of the program.  Loads all of the login components into the frame, here a user logs in to an already existing account.
	 */
	private void loadMainLogin(String errorOutputDefaultText) {
		if(frame == null)
			frame = new JFrame();
		
		// sets the UI to the system's native look and feel
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		frame.setTitle("Login");
		frame.setBounds(400, 150, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 1, 0, 0));
		
		JPanel accountPanel = new JPanel();
		frame.getContentPane().add(accountPanel);
		accountPanel.setLayout(new GridLayout(0,1, 0, 0));
		
		usernamePanel = new JPanel();
		accountPanel.add(usernamePanel);
		
		JLabel usernameLabel = new JLabel("Username");
		usernamePanel.add(usernameLabel);
		
		usernameField = new JTextField();
		usernamePanel.add(usernameField);
		usernameField.setColumns(10);
		
		passwordPanel = new JPanel();
		accountPanel.add(passwordPanel);
		
		JLabel passwordLabel = new JLabel("Password");
		passwordPanel.add(passwordLabel);
		
		passwordField = new JPasswordField();
		passwordField.setColumns(10);
		passwordPanel.add(passwordField);
		
		loginButton = new JButton("Login");
		loginButton.addActionListener(new LoginButtonActionListener());
		accountPanel.add(loginButton);

		frame.getRootPane().setDefaultButton(loginButton);
		createAccountPanel = new JPanel();
		accountPanel.add(createAccountPanel);
		
		createAccountLabel = new JLabel("New? Create an account");
		createAccountPanel.add(createAccountLabel);
		
		createAccountButton = new JButton("Create Account");
		createAccountButton.addActionListener(new CreateAccountButtonActionListener());
		createAccountPanel.add(createAccountButton);
		
		errorOutputLabel = new JLabel(errorOutputDefaultText);
		accountPanel.add(errorOutputLabel);
		errorOutputLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		frame.validate();
		//frame.getContentPane().invalidate();
		//frame.getContentPane().repaint();
	}
	
	/**
	 * loadUsernamePasswordGUI is used to fill the frame with components that allow the user to create a username and password.
	 */
	private void loadUsernamePasswordGUI() {
		frame.setTitle("Create Account");
		frame.setBounds(400, 150, 470, 272);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane();
		frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 20));
		
		loginInformationPanel = new JPanel();
		frame.getContentPane().add(loginInformationPanel);
		loginInformationPanel.setLayout(new GridLayout(5, 2, 0, 0));
		
		createAccountBackButton = new JButton("Back");
		createAccountBackButton.addActionListener(new CreateAccountBackButtonActionListener());
		loginInformationPanel.add(createAccountBackButton);
		
		JPanel createUsernamePanel = new JPanel();
		loginInformationPanel.add(createUsernamePanel);
		createUsernamePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel createUsernameLabel = new JLabel("Enter A Username  ");
		createUsernameLabel.setHorizontalAlignment(SwingConstants.TRAILING);
		createUsernamePanel.add(createUsernameLabel);
		
		createUsernameField = new JTextField();
		createUsernamePanel.add(createUsernameField);
		createUsernameField.setColumns(10);
		
		createPasswordPanel = new JPanel();
		loginInformationPanel.add(createPasswordPanel);
		createPasswordPanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		createPasswordLabel = new JLabel("Enter A Password   ");
		createPasswordLabel.setHorizontalAlignment(SwingConstants.TRAILING);
		createPasswordPanel.add(createPasswordLabel);
		
		createPasswordField = new JPasswordField();
		createPasswordPanel.add(createPasswordField);
		createPasswordField.setColumns(10);
		
		verticalSpacePanel_1 = new JPanel();
		loginInformationPanel.add(verticalSpacePanel_1);
		verticalSpacePanel_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		errorFeedBackLabel = new JLabel();
		errorFeedBackLabel.setHorizontalAlignment(SwingConstants.CENTER);
		verticalSpacePanel_1.add(errorFeedBackLabel);
		
		submitUsernamePasswordButton = new JButton("Submit");
		submitUsernamePasswordButton.addActionListener(new SubmitUsernamePasswordButtonActionListener());
		loginInformationPanel.add(submitUsernamePasswordButton);
		frame.getRootPane().setDefaultButton(submitUsernamePasswordButton);
		
		frame.validate();
		//frame.getContentPane().invalidate();
		//frame.getContentPane().repaint();
		
	}
	
	/**
	 * Implements the frame with components to input user information such as name, SSN, address, and phone number.
	 */
	private void loadUserInformationGUI() {
		frame.setTitle("Create Account");
		frame.setBounds(400, 150, 600, 500);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(4, 1, 0, 10));
		
		exitPersonalInformationPanel = new JPanel();
		frame.getContentPane().add(exitPersonalInformationPanel);
		exitPersonalInformationPanel.setLayout(new GridLayout(1, 1, 200, 0));
		
		submitUserBackButton = new JButton("Back");
		submitUserBackButton.addActionListener(new CreateAccountButtonActionListener());
		exitPersonalInformationPanel.add(submitUserBackButton);
		
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new CancelButtonActionListener());
		exitPersonalInformationPanel.add(cancelButton);
		
		enterNamePanel = new JPanel();
		frame.getContentPane().add(enterNamePanel);
		enterNamePanel.setLayout(new GridLayout(0, 3, 0, 0));
		
		firstNamePanel = new JPanel();
		enterNamePanel.add(firstNamePanel);
		firstNamePanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		firstNameLabel = new JLabel("Enter First Name");
		firstNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		firstNamePanel.add(firstNameLabel);
		
		firstNameField = new JTextField();
		firstNameField.setHorizontalAlignment(SwingConstants.CENTER);
		firstNamePanel.add(firstNameField);
		firstNameField.setColumns(10);
		
		middleNamePanel = new JPanel();
		enterNamePanel.add(middleNamePanel);
		middleNamePanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		middleNameLabel = new JLabel("Enter Middle Name");
		middleNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		middleNamePanel.add(middleNameLabel);
		
		middleNameField = new JTextField();
		middleNameField.setHorizontalAlignment(SwingConstants.CENTER);
		middleNamePanel.add(middleNameField);
		middleNameField.setColumns(10);
		
		lastNamePanel = new JPanel();
		enterNamePanel.add(lastNamePanel);
		lastNamePanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		lastNameLabel = new JLabel("Enter Last Name");
		lastNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lastNamePanel.add(lastNameLabel);
		
		lastNameField = new JTextField();
		lastNameField.setHorizontalAlignment(SwingConstants.CENTER);
		lastNamePanel.add(lastNameField);
		lastNameField.setColumns(10);
		
		personalInformationPanel = new JPanel();
		frame.getContentPane().add(personalInformationPanel);
		personalInformationPanel.setLayout(new GridLayout(0, 3, 0, 0));
		
		ssnPanel = new JPanel();
		personalInformationPanel.add(ssnPanel);
		ssnPanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		ssnLabel = new JLabel("Enter SSN");
		ssnLabel.setHorizontalAlignment(SwingConstants.CENTER);
		ssnPanel.add(ssnLabel);
		
		ssnField = new JTextField();
		ssnField.setHorizontalAlignment(SwingConstants.CENTER);
		ssnPanel.add(ssnField);
		ssnField.setColumns(10);
		
		addressPanel = new JPanel();
		personalInformationPanel.add(addressPanel);
		addressPanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		addressLabel = new JLabel("Enter Address");
		addressLabel.setHorizontalAlignment(SwingConstants.CENTER);
		addressPanel.add(addressLabel);
		
		addressField = new JTextField();
		addressField.setHorizontalAlignment(SwingConstants.CENTER);
		addressPanel.add(addressField);
		addressField.setColumns(10);
		
		phonePanel = new JPanel();
		personalInformationPanel.add(phonePanel);
		phonePanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		phoneLabel = new JLabel("Enter Phone Number");
		phoneLabel.setHorizontalAlignment(SwingConstants.CENTER);
		phonePanel.add(phoneLabel);
		
		phoneField = new JTextField();
		phoneField.setHorizontalAlignment(SwingConstants.CENTER);
		phonePanel.add(phoneField);
		phoneField.setColumns(10);
		
		//errorFeedbackPanel = new JPanel();
		//frame.getContentPane().add(errorFeedbackPanel);
		//errorFeedbackPanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		submitPanel = new JPanel();
		frame.getContentPane().add(submitPanel);
		submitPanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		errorFeedbackLabel = new JLabel();
		errorFeedbackLabel.setHorizontalAlignment(SwingConstants.CENTER);
		submitPanel.add(errorFeedbackLabel);
		
		submitUserButton = new JButton("Submit User Information");
		submitUserButton.addActionListener(new SubmitUserButtonActionListener());
		submitPanel.add(submitUserButton);
		frame.getRootPane().setDefaultButton(submitUserButton);
		
		firstNameField.requestFocus();
		
		frame.validate();
		//frame.getContentPane().invalidate();
		//frame.getContentPane().repaint();
		
	}
	
	private void loadPatientOrProvider() {
		// TODO Auto-generated method stub
		frame.setTitle("Patient or Provider");
		frame.setBounds(400, 150, 372, 226);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(5, 1, 0, 0));
		
		JLabel patientOrProviderLabel = new JLabel("<html><header style=\"font-family:cambria\"><font size=5>Are you a patient or provider?</font></header></html>");
		patientOrProviderLabel.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(patientOrProviderLabel);
		
		JButton patientButton = new JButton("Patient");
		patientButton.addActionListener(new PatientButtonActionListener());
		
		chooseTypeErrorOutputLabel = new JLabel("New label");
		chooseTypeErrorOutputLabel.setVisible(false);
		frame.getContentPane().add(chooseTypeErrorOutputLabel);
		frame.getContentPane().add(patientButton);
		
		JButton providerButton = new JButton("Provider");
		providerButton.addActionListener(new ProviderButtonActionListener());
		frame.getContentPane().add(providerButton);
		
		JButton cancelPatientProviderButton = new JButton("Cancel");
		cancelPatientProviderButton.addActionListener(new CancelButtonActionListener());
		frame.getContentPane().add(cancelPatientProviderButton);
	}
	
	private void loadChoosePrimaryProvider() {
		frame.setTitle("Choose Primary Provider");
		frame.setBounds(400, 150, 425, 262);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(4, 1, 0, 0));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel);
		panel.setLayout(new GridLayout(2, 1, 0, 2));
		
		String[] providerList = getProviderList();
		comboBox = new JComboBox<String>(providerList);
		comboBox.setToolTipText("Select a provider from the list of providers below");
		panel.add(comboBox);
		
		verticalStrut_3 = Box.createVerticalStrut(20);
		frame.getContentPane().add(verticalStrut_3);
		
		chooseProviderButton = new JButton("Choose Provider");
		chooseProviderButton.addActionListener(new ChooseProviderButtonActionListener());
		frame.getContentPane().add(chooseProviderButton);
		
		JButton cancelChooseProviderButton = new JButton("Cancel");
		cancelChooseProviderButton.addActionListener(new CancelButtonActionListener());
		frame.getContentPane().add(cancelChooseProviderButton);
	}
	
	private String[] getProviderList() {
		ArrayList<Provider> providers = Provider.getCurrentProviders();
		String[] providerList = new String[providers.size()];
		
		for(int i = 0; i < providers.size(); ++i) {
			providerList[i] = providers.get(i).getName();
		}
		
		return providerList;
	}
	
	/**
	 * private inline class that is called upon the submitButton being pressed.
	 *
	 */
	private class LoginButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			boolean login = false;
			byte type = 2;
			username = usernameField.getText();
			password = passwordField.getPassword();
			List<Account> accounts = Account.getCurrentAccounts();
			if(accounts != null)
				for(Account a : accounts) {
					if(a.getUsername().equals(username) && passwordCorrect(a.getPassword())) {
						login = true;
						ID = a.getUserID();
						type = a.getAccountType();
						break;//#marked to be reconsidered. should I let this loop run without breaking?
					}
				}
			accounts = null;//#marked to be reconsidered.  Potentially keep in RAM.
			if(login == true && type == (byte) 0) {
				PatientGUI pGUI = new PatientGUI(ID);
				pGUI.frame.setVisible(true);
			//	errorOutputLabel.setText("Error Output Here");
				errorOutputLabel.setForeground(Color.BLACK);
				frame.dispose();
			}
			else if(login == true && type == (byte) 1) {
				ProviderGUI pGUI = new ProviderGUI(ID);
				pGUI.frame.setVisible(true);
				errorOutputLabel.setText("Error Output Here");
				errorOutputLabel.setForeground(Color.BLACK);
				frame.dispose();
				
			}else if(login == true && type == (byte) 2) {
				clearFrame();
				loadUserInformationGUI();
			}
			else { 
				errorOutputLabel.setText("An account with that username and password was not found.");
				errorOutputLabel.setForeground(Color.RED);
			}
			
		}
		
		private boolean passwordCorrect(char[] pass) {
			
			if(pass.length == password.length) {
				for(int i = 0; i < pass.length; ++i) 
					if(pass[i] != password[i])
						return false;
			} else
				return false;
			return true;
			
		}
	}
	
	/**
	 * private inline class that is called upon the createAccount button being pressed.
	 */
	private class CreateAccountButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clearFrame();
			loadUsernamePasswordGUI();
		}
	}
	
	/**
	 * private inline class that is called upon the submitButton being pressed.
	 */
	private class SubmitUsernamePasswordButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			Border defaultBorder = UIManager.getBorder("TextField.border");
			String tempUsername = createUsernameField.getText();
			char[] tempPassword = createPasswordField.getPassword();
			boolean accept = true;
			boolean usernameAccept = true;
			//boolean passwordAccept = true;
			CharSequence[] blackListedCharacters = new CharSequence[] { ",", "\\", "/", "\'", "\"", " ", "\t", ":", ";" };
			if(checkEmptyUsername(tempUsername) && checkEmptyPassword(tempPassword)) {
				for(CharSequence cs : blackListedCharacters) {
					if(tempUsername.contains(cs)) {
						accept = false;
						usernameAccept = false;
						createUsernameField.setBorder(BorderFactory.createLineBorder(Color.RED, 1));
						errorFeedBackLabel.setText("<html>Fields with a <font color='red'>red box</font> are marked for containing invalid characters.</html>");
						break;
					}
				}
				/*for(char c : tempPassword) {
					for(CharSequence cs : blackListedCharacters) {
						if(c == cs.charAt(0)) {
							accept = false;
							passwordAccept = false;
							createPasswordField.setBorder(BorderFactory.createLineBorder(Color.RED, 1));
							errorFeedBackLabel.setText("<html>Fields with a <font color='red'>red box</font> are marked for containing invalid characters.</html>");
						}
					}
				}*/
				if(usernameAccept == true)
					createUsernameField.setBorder(defaultBorder);
				/*if(passwordAccept == true)
					createPasswordField.setBorder(defaultBorder);*/
				if(accept && checkUniqueUsername(tempUsername)) {
					username = createUsernameField.getText();
					password = createPasswordField.getPassword();//use getPassword(); in deliverable 3.
					clearFrame();
					loadUserInformationGUI();
				}
			}
			else{
				errorFeedBackLabel.setText("You cannot submit empty space as your username or password.");
			}
			
		}
		private boolean checkEmptyUsername(String checkUsername) {
			boolean valid = true;
			if(checkUsername.equals(""))
				return valid = false;
			return valid;
		}
		
		private boolean checkEmptyPassword(char[] checkPassword) {
			boolean valid = true;
			if(checkPassword.length == 0)
				return false;
			return valid;
		}
	}
		
		/**
		 * Method that checks to see if the user has input a usable username.
		 * @return returns whether or not the user input an already used username.  If return is false the username is already taken.
		 */
		private boolean checkUniqueUsername(String checkUsername) {
			// TODO Auto-generated method stub
			boolean unique = true;
			ArrayList<Account> accounts = Account.getCurrentAccounts();
			if(accounts != null)
				for(Account a : accounts) {
					if(a.getUsername().equals(checkUsername)) {
						accounts = null;
						errorFeedBackLabel.setText("This username is already in use.");
						return unique = false;
					}
					else {
						accounts = null;
						unique = true;
					}
				}
			else {
				//handle account = null error;
			}
			return unique;
			
		}
	
	
	/**
	 * private in-line class that is called upon the submitUserInformationButton being pressed.
	 */
	private class SubmitUserButtonActionListener implements ActionListener {
		String errorOutput = "";
		Border defaultBorder = UIManager.getBorder("TextField.border");
		public void actionPerformed(ActionEvent arg0) {
			boolean acceptInformation = checkForValidFields();
			if(acceptInformation) {
				userInformation = new String[4];
				userInformation[0] = firstNameField.getText() + ", " + middleNameField.getText() + 
						", " + lastNameField.getText();
				userInformation[1] = ssnField.getText();
				userInformation[2] = addressField.getText();
				userInformation[3] = phoneField.getText();
				clearFrame();
				loadPatientOrProvider();
			}
			else {
				errorFeedbackLabel.setText("<html>" + errorOutput + "</html>");
			}
			errorOutput = ""; // reset previously stored error message
			//loadMainLogin();
		}
		
		private boolean isNumber(String toCheck) {
			for(int i = 0; i < toCheck.length(); ++i)
			{
				if(!Character.isDigit(toCheck.charAt(i)))
					return false;
			}
			
			return true;
		}
		
		private boolean checkForValidFields() {
			boolean accept = true;
			String appendGreen = "", appendRed = "", appendBlue = "", appendYellow = "";
			JTextField[] fields = new JTextField[] {firstNameField, lastNameField, ssnField, addressField, phoneField };
			CharSequence[] blackListedCharacters = new CharSequence[] {"/", "\\", " ", "\'", ":", "\""};
			CharSequence[] blackListedAddressChars = new CharSequence[] {"/", "\\", "\'", ":", "\""};
			CharSequence[] blackList;
			boolean[] validCharacters = new boolean[] {true, true, true, true, true};
			for(int i = 0; i < fields.length; ++i) {
				String information = fields[i].getText();
				
				// checking for empty fields, except SSN which checks specifically for 9 chars
				if(information.length() < 1 && i != 2) {
					fields[i].setBorder(BorderFactory.createLineBorder(Color.GREEN, 1));
					appendGreen = "<p>You must fill in boxes that have a <font color='green'>green border.</font></p>";
					validCharacters[i] = false;
					accept = false;
				}
				if(i == 3)
					blackList = blackListedAddressChars;
				else
					blackList = blackListedCharacters;
				
				for(CharSequence c : blackList)
					if(information.contains(c)) {
						fields[i].setBorder(BorderFactory.createLineBorder(Color.RED, 1));
						appendRed = "<p>Boxes with a <font color='red'>red border</font> contain invalid characters.</p>";
						accept = false;
						validCharacters[i] = false;
						break;
					}
				
				if(i == 2) {
					int ssnLength = fields[i].getText().length();
					if(ssnLength != 9 || !isNumber(fields[i].getText())) {
						fields[i].setBorder(BorderFactory.createLineBorder(Color.BLUE, 1));
						appendBlue = "<p>Your <font color='blue'>SSN</font> should contain 9 numbers.</p>";
						accept = false;
						validCharacters[i] = false;
					}
				}
				
				if (i == 4){
					int phoneLength = fields[i].getText().length();
					if (phoneLength != 10 || !isNumber(fields[i].getText())){
						fields[i].setBorder(BorderFactory.createLineBorder(Color.ORANGE, 1));
						appendYellow = "<p>Your <font color='orange'>Phone Number</font> should contain 10 numbers.</p>";
						accept = false;
						validCharacters[i] = false;
					}
				}
			}
			
			// set the textfield borders, for which error-checking went smoothly, back to default
			for(int i = 0; i < validCharacters.length; ++i)
				if(validCharacters[i])
					fields[i].setBorder(defaultBorder);
			
			// append colored error labels to final error output message
			// if no error for a given color is unused, then the corresponding label is empty and is appended as an empty string
			errorOutput += appendGreen + appendBlue + appendRed + appendYellow;
			
			return accept;
		}
	}
	private class PatientButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			patientPressed();
		}
		
		private void patientPressed() {
			if(new File("provider.ser").exists()) {
				clearFrame();
				loadChoosePrimaryProvider();
			}
			else {
				chooseTypeErrorOutputLabel.setText("<html><font color='red'>There must be at least one provider before a patient can be added.</font></html>");
				chooseTypeErrorOutputLabel.setHorizontalAlignment(SwingConstants.CENTER);
				chooseTypeErrorOutputLabel.setVisible(true);
			}
		}		
	}
	private class ProviderButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			providerPressed();
			clearFrame();
			loadMainLogin("Account created succesfully");
		}
		
		private void providerPressed() {
			// this block serializes the account
			int generatedID = Account.generateUserID();
			Account account = new Account(username, password, generatedID);
			ID = generatedID;
			account.serializeObject(-1);
			account = null;
			
			// this block serializes the provider
			Provider provider = new Provider(userInformation, ID);
			provider.serializeObject(-1);
			setAccountType();
		}
		
		private void setAccountType() {
			ArrayList<Account> accounts = Account.getCurrentAccounts();
			
			Account account = accounts.get(ID - 1);
			account.setUserType((byte) 1);
			account.serializeObject(ID - 1);
			
			accounts = null;
			
			accounts = Account.getCurrentAccounts();
			
			for(Account duhAccount : accounts) {
				duhAccount.output();
			}
		}
	}
	
	private class CreateAccountBackButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clearFrame();
			loadMainLogin("");
		}
	}
	
	private class CancelButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			cancel();
		}
		
		private void cancel() {			
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			Login window = new Login("Cancelled account creation.");
			window.frame.setVisible(true);			
			frame.dispose();
		}
	}
	
	private class ChooseProviderButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			chooseProviderButtonPressed();
		}
		
		private void chooseProviderButtonPressed() {
			
			// this block serializes the account
			int generatedID = Account.generateUserID();
			Account seraccount = new Account(username, password, generatedID);
			ID = generatedID;
			seraccount.serializeObject(-1);
			seraccount = null;
			ArrayList<Account> accounts = Account.getCurrentAccounts();
			
			Account account = accounts.get(ID - 1);
			account.setUserType((byte) 0);
			account.serializeObject(ID - 1);
			
			accounts = null;
			
			accounts = Account.getCurrentAccounts();
			
			for(Account duhAccount : accounts) {
				duhAccount.output();
			}
			
			// this block serializes the patient
			int ppID = resolveIDOfSelection((String) comboBox.getSelectedItem());//primary provider ID.
			Patient patient = new Patient(userInformation, ID, ppID);
			patient.serializeObject(-1);
			patient.output();
			clearFrame();
			loadMainLogin("Account created successfully");
			
		}
		
		private int resolveIDOfSelection(String providerName) {
			ArrayList<Provider> providers = Provider.getCurrentProviders();
			int ppID = 0;
			System.out.println("Value taken is " + providerName);
			for(int i = 0; i < providers.size(); ++i) {
				if(providers.get(i).getName().equals(providerName)) {
					ppID = providers.get(i).getID();
					providers.get(i).addPatient(ID);
					providers.get(i).serializeObject(i);
					providers.get(i).output();
					i = providers.size(); //Ensures the loop ends naturally without a break;
				}
			}
			return ppID;
		}
		
	}
}
