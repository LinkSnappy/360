package hospital;

import junit.framework.TestCase;

public class PatientTest extends TestCase {
	
	public void testConstructor(){
		String[] input1 = {"name", "123456", "the hub", "8675309"};
		Patient pat1 = new Patient(input1, 123456, 0);
		
		assertEquals(true, (pat1 instanceof Patient));

		String[] input2 = {"", "", "", ""};
		Patient pat2 = new Patient(input2, 0, 0);
		assertEquals(true, (pat2 instanceof Patient));
	}
	
	public void testSettersAndGetters(){
		String[] input1 = {"name", "123456", "the hub", "8675309"};
		Patient pat1 = new Patient(input1, 123456, 0);
		
		pat1.setName("newName");
		String nameResult = pat1.getName();
		assertEquals("newName", nameResult);
		
		pat1.setSSN("654321");
		String ssnResult = pat1.getSSN();
		assertEquals("654321", ssnResult);
		
		pat1.setAddress("922 place");
		String addrResult = pat1.getAddress();
		assertEquals("922 place", addrResult);
		
		pat1.setPhone("10101010");
		String phoneResult = pat1.getPhone();
		assertEquals("10101010", phoneResult);
		
		pat1.setID(666);
		int testID = pat1.getID();
		assertEquals(666, testID);
		
	}
	
	public void testPatientComparator(){
		String[] input1 = {"name", "123456", "the hub", "8675309"};
		Patient pat1 = new Patient(input1, 1, 0);
		Patient pat2 = new Patient(input1, 2, 0);
		PatientComparator comparator = new PatientComparator();
		
		int result = comparator.compare(pat1, pat2);
		assertEquals(-1, result);
		
		result = comparator.compare(pat2, pat1);
		assertEquals(1, result);
		
		pat2.setID(1);
		result = comparator.compare(pat2, pat1);
		assertEquals(-1, result);

		result = comparator.compare(pat1, pat2);
		assertEquals(-1, result);
	}
}
