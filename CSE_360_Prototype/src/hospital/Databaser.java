package hospital;

import java.util.ArrayList;


public interface Databaser<T> {
	ArrayList<T> deserializeObjects();
	Patient getPatientData();
	void output();
	boolean serializeObject(int index);
}
