package hospital;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class ViewPrescriptionWindow {

	protected JFrame frame;

	/**
	 * Create the application.
	 */
	public ViewPrescriptionWindow(Issue issue) {
		initialize(issue);
	}

	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.entryPoint
	 */
	private void initialize(Issue issue) {
		if(frame == null)
			frame = new JFrame();
		frame.setBounds(100, 100, 350, 120);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 1, 0, 0));
		frame.getContentPane().add(new JLabel("Prescription: " + issue.getPrescription()));
		
	}

}
