package hospital;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.GridLayout;

import javax.swing.JPanel;

public class PatientGUI {
	/*Put into homescreen:
	 * View history.
	 * Manage Account.
	 * Logout Button.
	 * 
	 */
	protected JFrame frame;
	JLabel outputLabel;
	private Patient matchedPatient;
	private int index;
	private ViewHistoryWindow viewHistory;
	private ManageAccountGUI manageAccount;
	private CreateIssueGUI createIssue;
	//private PatientGUI oldPatientGUI;

	/**
	 * Create the application.
	 */
	public PatientGUI(int id) {
		matchedPatient = getmatchedPatient(id);
		viewHistory = null;
		patientMainMenu();
	}
	
	private void closeOtherFrames() {
		if(manageAccount != null) {
			if(manageAccount.frame != null)
				manageAccount.frame.dispose();
			manageAccount = null;
		}
		
		if(viewHistory != null) {
			if(viewHistory.frame != null) {
				viewHistory.frame.dispose();
				viewHistory.frame = null;
			}
			viewHistory = null;
		}
		
		if(createIssue != null) {
			if(createIssue.frame != null)
				createIssue.frame.dispose();
			createIssue = null;
			}
			
	}
	
	public Patient getmatchedPatient(int id) {
		List<Patient> patients = Patient.getCurrentPatients();
		if(patients != null)
			for(int i = 0; i < patients.size(); ++i/*Patient p : patients*/) {
				if(patients.get(i).getID() == id){
					index = i;
					return patients.get(i);
				}
			}
		index = -1;
		return null;
	}
	
	private void patientMainMenu() { 
		String patientLabelText = getTimeReply();
		if(frame == null)
			frame = new JFrame();
		frame.setTitle("Main Menu");
		frame.setBounds(400, 150, 320, 260);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel patientHomescreen = new JPanel();
		frame.getContentPane().add(patientHomescreen);
		patientHomescreen.setLayout(new GridLayout(5, 0, 0, 0));
		
		JButton createIssueButton = new JButton("Create Issue");
		createIssueButton.addActionListener(new CreateIssueButtonActionListener());
		
		JLabel lblNewLabel = new JLabel(patientLabelText);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		patientHomescreen.add(lblNewLabel);
		patientHomescreen.add(createIssueButton);
		
		JButton viewHistory = new JButton("View History");
		viewHistory.addActionListener(new ViewHistoryActionListener());
		patientHomescreen.add(viewHistory);
		
		JButton manageAccount = new JButton("Manage Account");
		manageAccount.addActionListener(new ManageAccountActionListener());
		patientHomescreen.add(manageAccount);
		
		JButton logoutButton = new JButton("Logout");
		logoutButton.addActionListener(new LogoutButtonActionListener());
		patientHomescreen.add(logoutButton);
	}
	private String getTimeReply() {
		// TODO Auto-generated method stub
		String patientLabelText = null;
		int time = -1;
		Calendar cal = Calendar.getInstance();
    	cal.getTime();
    	SimpleDateFormat sdf = new SimpleDateFormat("HH");
    	System.out.println( sdf.format(cal.getTime()) );
    	time = Integer.valueOf(sdf.format( cal.getTime()));
    	//System.out.println(time);
    	String[] name = matchedPatient.getName().split(",");
		if(time >= 18)
			patientLabelText = "Good evening, ";
		else if(time >= 12)
			patientLabelText = "Good afternoon, ";
		else 
			patientLabelText = "Good morning, ";
		patientLabelText += name[0];
		return patientLabelText;
	}
	
	/**
	 * initializes the frame so that the user can input a head related issue.
	 */
	
	private class CreateIssueButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			createIssueButtonPressed();
		}
		
		private void createIssueButtonPressed() {
			if(createIssue == null) {
				closeOtherFrames();
				createIssue = new CreateIssueGUI(index, matchedPatient);
				createIssue.frame.setBounds(frame.getX() + frame.getWidth() + 5, frame.getY(), 320, 260);
				createIssue.frame.setVisible(true);
			}
			else {
				closeOtherFrames();
				createIssue = new CreateIssueGUI(index, matchedPatient);
				createIssue.frame.setBounds(frame.getX() + frame.getWidth() + 5, frame.getY(), 320, 260);
				createIssue.frame.setVisible(true);
			}
		}
	}
	private class ViewHistoryActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			viewHistoryButtonPressed();
		}
		
		private void viewHistoryButtonPressed() {
				closeOtherFrames();
				viewHistory = new ViewHistoryWindow(matchedPatient);
				viewHistory.frame.setBounds(frame.getX() + frame.getWidth() + 5, frame.getY(), 450, 300);
				viewHistory.frame.setVisible(true);
		}
	}
	private class ManageAccountActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
				closeOtherFrames();
				manageAccount = new ManageAccountGUI(matchedPatient.getID(), matchedPatient, frame);
				////frame.setBounds(100, 100, 357, 156);
				manageAccount.frame.setBounds(frame.getX() + frame.getWidth() + 5, frame.getY(), 357, 260);
				manageAccount.frame.setVisible(true);
		}
	}
	private class LogoutButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			logout();
		}
		
		private void logout() {
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			matchedPatient = null;
			index = 0;
			Login window = new Login("");
			window.frame.setVisible(true);
			
			closeOtherFrames();
			
			frame.dispose();
		}
	}
}
