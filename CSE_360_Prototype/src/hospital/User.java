package hospital;


public class User implements java.io.Serializable {
	 private String name;
	 private int ID;
	 private String SSN;
	 private String accountNumber;
	 private String address;
	 private String phone;
	 private static final long serialVersionUID = 1L;
	 
	/**
	 * @return the name
	 */
	protected String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	protected void setName(String toName) {
		this.name = toName;
	}
	
	/**
	 * @return the iD
	 */
	protected int getID() {
		return ID;
	}
	
	/**
	 * @param iD the iD to set
	 */
	protected void setID(int toID) {
		ID = toID;
	}
	
	/**
	 * @return the sSN
	 */
	protected String getSSN() {
		return SSN;
	}
	
	/**
	 * @param sSN the sSN to set
	 */
	protected void setSSN(String toSSN) {
		SSN = toSSN;
	}
	
	/**
	 * @return the accountNumber
	 */
	protected String getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * @param accountNumber the accountNumber to set
	 */
	protected void setAccountNumber(String toAccountNumber) {
		this.accountNumber = toAccountNumber;
	}
	
	/**
	 * @return the address
	 */
	protected String getAddress() {
		return address;
	}
	
	/**
	 * @param address the address to set
	 */
	protected void setAddress(String toAddress) {
		this.address = toAddress;
	}
	
	/**
	 * @return the phone
	 */
	protected String getPhone() {
		return phone;
	}
	
	/**
	 * @param phone the phone to set
	 */
	protected void setPhone(String toPhone) {
		this.phone = toPhone;
	}

}
