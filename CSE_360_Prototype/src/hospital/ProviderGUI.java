package hospital;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;

public class ProviderGUI {

	protected JFrame frame, viewPatientsFrame;
	private Provider matchedProvider;
	private ManageAccountGUI manageAccount;
	private ViewPatientWindow vpw;
	private int providerID;
	/**
	 * Create the application.
	 * @param iD 
	 */
	public ProviderGUI(int ID) {
		providerID = ID;
		matchedProvider = getMatchedProvider(ID);
		loadProviderMenu();
	}
	
	private Provider getMatchedProvider(int ID) {
		ArrayList<Provider> providers = Provider.getCurrentProviders();
		if(providers != null)
			for(int i = 0; i < providers.size(); ++i) {
				if(providers.get(i).getID() == ID){
					return providers.get(i);
				}
			}
		return null;
	}
	
	private String getTimeReply() {
		// TODO Auto-generated method stub
		String providerLabelText = null;
		int time = -1;
		Calendar cal = Calendar.getInstance();
    	cal.getTime();
    	SimpleDateFormat sdf = new SimpleDateFormat("HH");
    	System.out.println( sdf.format(cal.getTime()) );
    	time = Integer.valueOf(sdf.format( cal.getTime()));
    	//System.out.println(time);
    	String[] name = matchedProvider.getName().split(",");
		if(time >= 18)
			providerLabelText = "Good evening, ";
		else if(time >= 12)
			providerLabelText = "Good afternoon, ";
		else
			providerLabelText = "Good morning, ";
		providerLabelText += name[0];
		return providerLabelText;
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void loadProviderMenu() {
		String providerLabelText = getTimeReply();
		if(frame == null)
			frame = new JFrame();
		frame.setTitle("Main Menu");
		frame.setBounds(400, 150, 320, 260);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 1, 0, 0));
		
		JPanel providerHomeScreen = new JPanel();
		frame.getContentPane().add(providerHomeScreen);
		providerHomeScreen.setLayout(new GridLayout(4, 0, 0, 0));
		
		JLabel lblNewLabel = new JLabel(providerLabelText);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		providerHomeScreen.add(lblNewLabel);
		
		JButton viewPatients = new JButton("View Patients");
		viewPatients.addActionListener(new ViewPatientsActionListener());
		providerHomeScreen.add(viewPatients);
		
		JButton manageAccount = new JButton("Manage Account");
		manageAccount.addActionListener(new ManageAccountActionListener());
		providerHomeScreen.add(manageAccount);
		
		JButton logout = new JButton("Logout");
		logout.addActionListener(new LogoutActionListener());
		providerHomeScreen.add(logout);
	}
	
	private void loadViewPatients() {
		if(viewPatientsFrame == null)
			viewPatientsFrame = new JFrame();
		viewPatientsFrame.setBounds(frame.getX() + frame.getWidth() + 5, frame.getY(), 450, 300);
		viewPatientsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		viewPatientsFrame.getContentPane().setLayout(new GridLayout(1, 1, 0, 0));
		
		
		JPanel scrollPanelContents = fillScrollContents();	
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(scrollPanelContents);
		scrollPane.setViewportBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		viewPatientsFrame.getContentPane().add(scrollPane);
		
	
		viewPatientsFrame.validate();
		viewPatientsFrame.setVisible(true);
	}
	
	private class ViewPatientsActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			closeOtherFrames();
			loadViewPatients();
		}
	}
	
	private class ManageAccountActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
				closeOtherFrames();
				manageAccount = new ManageAccountGUI(matchedProvider.getID(), matchedProvider, frame);
				manageAccount.frame.setBounds(frame.getX() + frame.getWidth() + 5, frame.getY(), 357, 260);
				manageAccount.frame.setVisible(true);
		}
	}
	
	private void closeOtherFrames()
	{
		if(viewPatientsFrame != null)
		{
			viewPatientsFrame.dispose();
			viewPatientsFrame = null;
		}
		
		// manageAccount will not have a frame if it's null
		if(manageAccount != null)
		{
			manageAccount.frame.dispose();
			manageAccount = null;
		}
	}
	
	private JPanel fillScrollContents() {
		ArrayList<Patient> patients = new ArrayList<Patient>();
		ArrayList<Patient> allPatients = Patient.getCurrentPatients();
		int iteration = 0;
		if(matchedProvider.patientIDs != null) {
			for(Integer integer : matchedProvider.patientIDs) {
				System.out.println("PatientID: " + integer);
				for(Patient patient : allPatients) {
					if(patient.getID() == integer) {
						patients.add(patient);
					}
				}
			}
			allPatients = null; //no longer needed.
		}
		if(patients.size() > 1)
			for(Patient patient: patients) {
				if(patient.issues != null)
					patient.issues = patient.InsertionSortIssue(patient.issues);
			}
		ArrayList<Patient> sortedPatients;
		System.out.println("Patients.size() = " +patients.size());
		if(patients != null)
			sortedPatients = InsertionSortPatientsWithoutClosedIssues(patients);/*InsertionSortPatients(patients);*/
		else 
			sortedPatients = new ArrayList<Patient>();
		JPanel patientListPanel = new JPanel();
		patientListPanel.setLayout(new GridLayout(sortedPatients.size(), 3, 0, 0));
		patientListPanel.setPreferredSize(new Dimension(100, 40 * sortedPatients.size()));
			for(Patient patient : sortedPatients) {
				int tempUrgency = 0;
				if(patient.issues.size() > 0)
					tempUrgency = patient.issues.get(0).getUrgency();
				iteration++;
				System.out.println(iteration);
				String[] name = patient.getName().split(",");
				JLabel patientName = new JLabel(name[2] + ", " + name[0]);
				JLabel mostUrgentIssue = new JLabel("Urgency: " + Integer.toString(tempUrgency));
				JButton viewPatientButton = new JButton();
				viewPatientButton.setText("Patient Issues");
				viewPatientButton.addActionListener(new ViewPatientButtonPressed(patient));
				patientListPanel.add(patientName);
				patientListPanel.add(mostUrgentIssue);
				patientListPanel.add(viewPatientButton);
			}
		return patientListPanel;
	}
	
	private ArrayList<Patient> InsertionSortPatients(ArrayList<Patient> dosePatients) {
		int i;
		int j;
		Patient key;
		
		if(dosePatients != null)
			for(i = dosePatients.size() - 1; i >= 0; --i) {
				if(dosePatients.get(i).issues.isEmpty()) //will remove any patients without issues.  This will prevent them from being displayed at all.
					dosePatients.remove(i);            //NOTE THAT THIS WILL NOT BE SERIALIZED SO DATA WILL NOT BE LOST.
			} 
		else
			return null;
		Patient[] patients = new Patient[dosePatients.size()];
		if(dosePatients != null)
			dosePatients.toArray(patients);
		if(dosePatients != null && dosePatients.size() > 1)
			dosePatients.clear();
		if(patients.length >= 2) {
			for (j = 1; j < patients.length; j++) {
				key = patients[j];
				System.out.println("j = " + j);
		    	for(i = (j-1); (i >= 0) && (patients[i].issues.get(0).getUrgency() < key.issues.get(0).getUrgency()); i--) {
		    		System.out.println("i = " + i);
		    		patients[i+1] = patients[i];
		    	}
		    	patients[i+1] = key;
		    }
		    
		    for(Patient patient : patients) {
		    	dosePatients.add(patient);
		    }
		}
		    
		   return dosePatients;
	}

	private ArrayList<Patient> InsertionSortPatientsWithoutClosedIssues(ArrayList<Patient> dosePatients) {
		for(int i = 0; i < dosePatients.size(); ++i) {
			for(int j = dosePatients.get(i).issues.size() - 1; j >= 0; --j) {
				if(dosePatients.get(i).issues.get(j).getStatus() == false)
					dosePatients.get(i).issues.remove(j);
			}
		}
		
		ArrayList<Patient> patients = InsertionSortPatients(dosePatients);
		return patients;
	}
	
	
	private class ViewPatientButtonPressed implements ActionListener {
		Patient patient;
		
		public ViewPatientButtonPressed(Patient p) {
			patient = p;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			vpw = new ViewPatientWindow(patient.getID(), providerID, frame, viewPatientsFrame);
			vpw.frame.setVisible(true);
			
		}
		
	}
	private class LogoutActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			closeOtherFrames();
			logout();
		}
		
		private void logout() {
			if(vpw != null) {
				vpw.frame.dispose();
				vpw = null;
			}
			Login login = new Login("");
			matchedProvider = null;
			frame.dispose();
			if(vpw != null) {
				if(vpw.pw != null) {
					if(vpw.pw.frame != null) {
						vpw.pw.frame.dispose();
					}
				}
				vpw = null;
			}
			
			login.frame.setVisible(true);
		}
	}
}
