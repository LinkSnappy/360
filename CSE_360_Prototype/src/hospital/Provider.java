package hospital;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@SuppressWarnings("rawtypes")
public class Provider extends User implements Databaser, java.io.Serializable {
	/*
	 * private String name;
	 * private String SSN;
	 * private String accountNumber;
	 * private String address;
	 * private String phone;
	 * private int ID;
	 * 
	 * Things to do for Provider: 
	 * Homescreen:
	 * - Patient Lists.  Sorted by urgency.
	 * - Click on patient, open patients history.
	 * - Prescribe a treatment
	 * - Update File:
	 * - - Mark as closed.
	 */
	protected ArrayList<Integer> patientIDs; //A list of this provider's patients.  This is specific to each provider.
	//protected transient ArrayList<Patient> patients;
	private static final long serialVersionUID = 1L;
	
	public Provider(String[] fields, int toID) {
		super.setName(fields[0]);
		super.setSSN(fields[1]);
		super.setAddress(fields[2]);
		super.setPhone(fields[3]);
		super.setID(toID);
		patientIDs = new ArrayList<Integer>();
	}
	
	/* (non-Javadoc)
	 * @see prototype_unique_name.User#getName()
	 */
	@Override
	protected String getName() {
		// TODO Auto-generated method stub
		return super.getName();
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#setName(java.lang.String)
	 */
	@Override
	protected void setName(String toName) {
		// TODO Auto-generated method stub
		super.setName(toName);
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#getID()
	 */
	@Override
	protected int getID() {
		// TODO Auto-generated method stub
		return super.getID();
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#setID(java.lang.String)
	 */
	@Override
	protected void setID(int toID) {
		// TODO Auto-generated method stub
		super.setID(toID);
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#getSSN()
	 */
	@Override
	protected String getSSN() {
		// TODO Auto-generated method stub
		return super.getSSN();
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#setSSN(java.lang.String)
	 */
	@Override
	protected void setSSN(String toSSN) {
		// TODO Auto-generated method stub
		super.setSSN(toSSN);
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#getAccountNumber()
	 */
	@Override
	protected String getAccountNumber() {
		// TODO Auto-generated method stub
		return super.getAccountNumber();
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#setAccountNumber(java.lang.String)
	 */
	@Override
	protected void setAccountNumber(String toAccountNumber) {
		// TODO Auto-generated method stub
		super.setAccountNumber(toAccountNumber);
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#getAddress()
	 */
	@Override
	protected String getAddress() {
		// TODO Auto-generated method stub
		return super.getAddress();
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#setAddress(java.lang.String)
	 */
	@Override
	protected void setAddress(String toAddress) {
		// TODO Auto-generated method stub
		super.setAddress(toAddress);
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#getPhone()
	 */
	@Override
	protected String getPhone() {
		// TODO Auto-generated method stub
		return super.getPhone();
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#setPhone(java.lang.String)
	 */
	@Override
	protected void setPhone(String toPhone) {
		// TODO Auto-generated method stub
		super.setPhone(toPhone);
	}

	@Override
	public boolean serializeObject(int indexToRemove) {
		// TODO Auto-generated method stub
		boolean success = false;
		Provider provider = this;
		List<Provider> providers = null;
		providers = deserializeObjects();
		if(indexToRemove != -1 && providers != null){
			try {
				providers.remove(indexToRemove);
			} catch(IndexOutOfBoundsException ex) {
				System.out.println("Error trying to remove index: \"" + indexToRemove + "\"");
			}
		}
		if(providers == null)//will only occur if the file does not exist or has been made unreadable in some way.
				providers = new ArrayList<Provider>(); //instantiates a new ArrayList if deserializeObjects returns null (will only happen if the target file is file not found or empty.
		
		providers.add(provider);

		if(providers.size() > 1)
			Collections.sort(providers, new ProviderComparator()); // uses 
		try
	      {
			if(!(new File("provider.ser").exists())) 
				new File("provider.ser").createNewFile();
	         FileOutputStream fileOut =
	         new FileOutputStream("provider.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(providers);
	         out.close();
	         fileOut.close();
	         success = true;
	      }catch(IOException e)
	      {
	    	  success = false; //problem was caused and serialization 
	          e.printStackTrace();
	      }
		return success;
	}

	@Override
	public void output() {
		System.out.println("OUTPUT WAS REACHED");
		System.out.println("Deserialized Provider: ");
		System.out.println("Name: " + super.getName());
		System.out.println("Address: " + super.getAddress());
		System.out.println("SSN: " + super.getSSN());
		System.out.println("Number: " + super.getPhone());
		System.out.println("ID = " + super.getID());
		if(patientIDs != null) {
			for(Integer id : patientIDs) 
				System.out.println("patientIDs = " + id);
		}
		
	}

	@Override
	public Patient getPatientData() {
		return getPatientData(super.getID());
		//here
	}
	
	public static ArrayList<Provider> getCurrentProviders() {
		ArrayList<Provider> providers = null;
		FileInputStream fileIn = null;
		try {
			fileIn = new FileInputStream("provider.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			
			providers = (ArrayList<Provider>) in.readObject();
			in.close();
	        fileIn.close();
			
		}catch(EOFException  | FileNotFoundException e) {
	    	  e.printStackTrace();
		}catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
		}
		
		return providers;
	}
	
	private Patient getPatientData(int gotID) {
		return null;
		//here.
		
	}

	@Override
	public ArrayList<Provider> deserializeObjects() {
		// TODO Auto-generated method stubs
		ArrayList<Provider> patients = null;
		FileInputStream fileIn = null;
		try {
			fileIn = new FileInputStream("provider.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			
			patients = (ArrayList<Provider>) in.readObject();
			in.close();
	        fileIn.close();
			
		}catch(EOFException  | FileNotFoundException e) {
	    	  return null;
		}catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		return patients;
	}



	public void addPatient(int patientID) {
		if(patientIDs == null)
			patientIDs = new ArrayList<Integer>();
		patientIDs.add(patientID);
		/*ArrayList<Patient> tempPatients = Patient.getCurrentPatients();
		for(int i = 0; i < tempPatients.size(); ++i) {
			if(tempPatients.get(i).getID() == patientID)
				patients.add(tempPatients.get(i));
		}
		tempPatients = null;*/
		
	}
	
	/*public void addPatient(Patient patientToAdd) {
		if(patients == null)
			patients = new ArrayList<Patient>();
		patients.add(patientToAdd);
	}*/
}

class ProviderComparator implements Comparator<Provider> {

	@Override
	public int compare(Provider p1, Provider p2) {
		// TODO Auto-generated method stub
		if(p1.getID() > p2.getID())
			return 1;
		else 
			return -1;
	}
	
}

//For serialize encrypted:
/*SealedObject sealedObject = null;
Cipher cipher = null;
try {
	cipher = Cipher.getInstance("DES");
	cipher.init(Cipher.ENCRYPT_MODE, key);
	
	sealedObject = new SealedObject(provider, cipher);
} catch (IllegalBlockSizeException | NoSuchAlgorithmException
		| NoSuchPaddingException | IOException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
} catch (InvalidKeyException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}*/
