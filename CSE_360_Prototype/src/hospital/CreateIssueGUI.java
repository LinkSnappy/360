package hospital;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;


public class CreateIssueGUI {

	protected JFrame frame;
	private Patient matchedPatient;
	private JSlider breathSlider, nauseaSlider, headacheSlider, 
					painSlider, numbnessSlider, swellingSlider, 
					pressureSlider, heartburnSlider, chestpainSlider, 
					indigestionSlider, appetitelossSlider, constipationSlider;
	private JLabel outputLabel;
	private JButton submitHeadIssueButton, submitArmIssueButton, 
	submitChestIssueButton, submitStomachIssueButton, submitLegIssueButton;
	private int patientIndex;
	private final int MINIMUM = 0;
	private final int MAXIMUM = 10;
	
	/**
	 * Create the application.
	 * @param index 
	 */
	public CreateIssueGUI(int index, Patient patient) {
		matchedPatient = patient;
		patientIndex = index;
		loadPointToWhereItHurts();
	}

	private void clearFrame() {
		frame.getContentPane().removeAll();
		frame.validate();
	}
	
	/**
	 * loadsTheFrameThatBegins the users questioning.
	 */
	private void loadPointToWhereItHurts() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("Point To Where It Hurts");
		//frame.setBounds(100, 100, 320, 260);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		String[] patName = matchedPatient.getName().split(",");
		patName[1].replace(" ", "");
		patName[2].replace(" ", "");
		
		JLabel patientName;
		
		if(patName[1].length() < 1 || patName[1].equals(" "))
			patientName = new JLabel("Patient Name: " + patName[0] + ", " + patName[2]);
		else
			patientName = new JLabel("Patient Name: " + patName[0] + ", " + patName[1] + ", " + patName[2]);
		patientName.setHorizontalAlignment(SwingConstants.LEFT);
		patientName.setBounds(84, 11, 147, 14);
		/*if(matchedPatient != null)
			patientName.setText(matchedPatient.getName());
		else
			patientName.setText("Error patient not logged in");*/
		frame.getContentPane().add(patientName);
		
		JButton headButton = new JButton("Head");
		headButton.addActionListener(new HeadButtonActionListener());
		headButton.setBounds(109, 36, 89, 23);
		frame.getContentPane().add(headButton);
		
		JButton chestButton = new JButton("Chest");
		chestButton.addActionListener(new ChestButtonActionListener());
		chestButton.setBounds(109, 70, 89, 23);
		frame.getContentPane().add(chestButton);
		
		JButton arms1Button = new JButton("Arms");
		arms1Button.addActionListener(new ArmsButtonActionListener());
		arms1Button.setBounds(10, 81, 89, 23);
		frame.getContentPane().add(arms1Button);
		
		JButton arms2Button = new JButton("Arms");
		arms2Button.setBounds(208, 81, 89, 23);
		arms2Button.addActionListener(new ArmsButtonActionListener());
		frame.getContentPane().add(arms2Button);
		
		JButton stomachButton = new JButton("Stomach");
		stomachButton.addActionListener(new StomachButtonActionListener());
		stomachButton.setBounds(109, 95, 89, 23);
		frame.getContentPane().add(stomachButton);
		
		JButton legs1Button = new JButton("Legs");
		legs1Button.addActionListener(new LegsButtonActionListener());
		legs1Button.setBounds(84, 129, 69, 89);
		frame.getContentPane().add(legs1Button);
		
		JButton legs2Button = new JButton("Legs");
		legs2Button.addActionListener(new LegsButtonActionListener());
		legs2Button.setBounds(163, 129, 68, 89);
		frame.getContentPane().add(legs2Button);
		
		frame.getContentPane().validate();
	}
	
	private void loadChest() {
		frame.setTitle("Chest");
		frame.setBounds(frame.getX(), frame.getY(), 567, 438);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel questionsPanel = new JPanel();
		frame.getContentPane().add(questionsPanel);
		questionsPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel instructionsPanel = new JPanel();
		questionsPanel.add(instructionsPanel);
		instructionsPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel instructionLabel = new JLabel("On a severity scale from 0 through 10, rate your expression of the following symptoms:");
		instructionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		instructionsPanel.add(instructionLabel);
		
		JPanel questionOnePanel = new JPanel();
		questionsPanel.add(questionOnePanel);
		questionOnePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionOneLabel = new JLabel("Pressure");
		questionOnePanel.add(questionOneLabel);
		
		pressureSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		pressureSlider.setPaintTicks(true);
		pressureSlider.setPaintLabels(true);
		pressureSlider.setMinimum(MINIMUM);
		pressureSlider.setMaximum(MAXIMUM);
		pressureSlider.setMajorTickSpacing(1);
		questionOnePanel.add(pressureSlider);
		
		JPanel questionTwoPanel = new JPanel();
		questionsPanel.add(questionTwoPanel);
		questionTwoPanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionTwoLabel = new JLabel("Heartburn");
		questionTwoPanel.add(questionTwoLabel);
		
		heartburnSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		questionTwoPanel.add(heartburnSlider);
		heartburnSlider.setPaintTicks(true);
		heartburnSlider.setPaintLabels(true);
		heartburnSlider.setMinimum(MINIMUM);
		heartburnSlider.setMaximum(MAXIMUM);
		heartburnSlider.setMajorTickSpacing(1);
		
		JPanel questionThreePanel = new JPanel();
		questionsPanel.add(questionThreePanel);
		questionThreePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionThreeLabel = new JLabel("Chest Pain");
		questionThreePanel.add(questionThreeLabel);
		
		chestpainSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		chestpainSlider.setPaintTicks(true);
		chestpainSlider.setPaintLabels(true);
		chestpainSlider.setMinimum(MINIMUM);
		chestpainSlider.setMaximum(MAXIMUM);
		chestpainSlider.setMajorTickSpacing(1);
		questionThreePanel.add(chestpainSlider);
		
		JPanel questionFourPanel = new JPanel();
		questionsPanel.add(questionFourPanel);
		
		JPanel submitPanel = new JPanel();
		questionsPanel.add(submitPanel);
		submitPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		submitChestIssueButton = new JButton("Submit Issue");
		submitChestIssueButton.addActionListener(new SubmitChestIssueButtonActionListener());
		
		outputLabel = new JLabel("");
		outputLabel.setHorizontalAlignment(SwingConstants.CENTER);
		submitPanel.add(outputLabel);
		submitPanel.add(submitChestIssueButton);
	}
	
	private void loadStomach() {
		frame.setTitle("Stomach");
		frame.setBounds(frame.getX(), frame.getY(), 567, 438);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel questionsPanel = new JPanel();
		frame.getContentPane().add(questionsPanel);
		questionsPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel instructionsPanel = new JPanel();
		questionsPanel.add(instructionsPanel);
		instructionsPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel instructionLabel = new JLabel("On a severity scale from 0 through 10, rate your expression of the following symptoms:");
		instructionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		instructionsPanel.add(instructionLabel);
		
		JPanel questionOnePanel = new JPanel();
		questionsPanel.add(questionOnePanel);
		questionOnePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionOneLabel = new JLabel("Indigestion");
		questionOnePanel.add(questionOneLabel);
		
		indigestionSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		indigestionSlider.setPaintTicks(true);
		indigestionSlider.setPaintLabels(true);
		indigestionSlider.setMinimum(MINIMUM);
		indigestionSlider.setMaximum(MAXIMUM);
		indigestionSlider.setMajorTickSpacing(1);
		questionOnePanel.add(indigestionSlider);
		
		JPanel questionTwoPanel = new JPanel();
		questionsPanel.add(questionTwoPanel);
		questionTwoPanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionTwoLabel = new JLabel("Loss of Appetite");
		questionTwoPanel.add(questionTwoLabel);
		
		appetitelossSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		questionTwoPanel.add(appetitelossSlider);
		appetitelossSlider.setPaintTicks(true);
		appetitelossSlider.setPaintLabels(true);
		appetitelossSlider.setMinimum(MINIMUM);
		appetitelossSlider.setMaximum(MAXIMUM);
		appetitelossSlider.setMajorTickSpacing(1);
		
		JPanel questionThreePanel = new JPanel();
		questionsPanel.add(questionThreePanel);
		questionThreePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionThreeLabel = new JLabel("Constipation");
		questionThreePanel.add(questionThreeLabel);
		
		constipationSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		constipationSlider.setPaintTicks(true);
		constipationSlider.setPaintLabels(true);
		constipationSlider.setMinimum(MINIMUM);
		constipationSlider.setMaximum(MAXIMUM);
		constipationSlider.setMajorTickSpacing(1);
		questionThreePanel.add(constipationSlider);
		
		JPanel questionFourPanel = new JPanel();
		questionsPanel.add(questionFourPanel);
		
		JPanel submitPanel = new JPanel();
		questionsPanel.add(submitPanel);
		submitPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		submitStomachIssueButton = new JButton("Submit Issue");
		submitStomachIssueButton.addActionListener(new SubmitStomachIssueButtonActionListener());
		
		outputLabel = new JLabel();
		outputLabel.setHorizontalAlignment(SwingConstants.CENTER);
		submitPanel.add(outputLabel);
		submitPanel.add(submitStomachIssueButton);
	}
	
	private void loadArms() {
		frame.setTitle("Arm");
		frame.setBounds(frame.getX(), frame.getY(), 567, 438);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel questionsPanel = new JPanel();
		frame.getContentPane().add(questionsPanel);
		questionsPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel instructionsPanel = new JPanel();
		questionsPanel.add(instructionsPanel);
		instructionsPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel instructionLabel = new JLabel("On a severity scale from 0 through 10, rate your expression of the following symptoms:");
		instructionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		instructionsPanel.add(instructionLabel);
		
		JPanel questionOnePanel = new JPanel();
		questionsPanel.add(questionOnePanel);
		questionOnePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionOneLabel = new JLabel("Pain");
		questionOnePanel.add(questionOneLabel);
		
		painSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		painSlider.setPaintTicks(true);
		painSlider.setPaintLabels(true);
		painSlider.setMinimum(MINIMUM);
		painSlider.setMaximum(MAXIMUM);
		painSlider.setMajorTickSpacing(1);
		questionOnePanel.add(painSlider);
		
		JPanel questionTwoPanel = new JPanel();
		questionsPanel.add(questionTwoPanel);
		questionTwoPanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionTwoLabel = new JLabel("Swelling");
		questionTwoPanel.add(questionTwoLabel);
		
		swellingSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		questionTwoPanel.add(swellingSlider);
		swellingSlider.setPaintTicks(true);
		swellingSlider.setPaintLabels(true);
		swellingSlider.setMinimum(MINIMUM);
		swellingSlider.setMaximum(MAXIMUM);
		swellingSlider.setMajorTickSpacing(1);
		
		JPanel questionThreePanel = new JPanel();
		questionsPanel.add(questionThreePanel);
		questionThreePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionThreeLabel = new JLabel("Numbness");
		questionThreePanel.add(questionThreeLabel);
		
		numbnessSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		numbnessSlider.setPaintTicks(true);
		numbnessSlider.setPaintLabels(true);
		numbnessSlider.setMinimum(MINIMUM);
		numbnessSlider.setMaximum(MAXIMUM);
		numbnessSlider.setMajorTickSpacing(1);
		questionThreePanel.add(numbnessSlider);
		
		JPanel questionFourPanel = new JPanel();
		questionsPanel.add(questionFourPanel);
		
		JPanel submitPanel = new JPanel();
		questionsPanel.add(submitPanel);
		submitPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		submitArmIssueButton = new JButton("Submit Issue");
		submitArmIssueButton.addActionListener(new SubmitArmLegIssueButtonActionListener());
		
		outputLabel = new JLabel();
		outputLabel.setHorizontalAlignment(SwingConstants.CENTER);
		submitPanel.add(outputLabel);
		submitPanel.add(submitArmIssueButton);
	}
	
	private void loadLegs() {
		frame.setTitle("Leg");
		frame.setBounds(frame.getX(), frame.getY(), 567, 438);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel questionsPanel = new JPanel();
		frame.getContentPane().add(questionsPanel);
		questionsPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel instructionsPanel = new JPanel();
		questionsPanel.add(instructionsPanel);
		instructionsPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel instructionLabel = new JLabel("On a severity scale from 0 through 10, rate your expression of the following symptoms:");
		instructionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		instructionsPanel.add(instructionLabel);
		
		JPanel questionOnePanel = new JPanel();
		questionsPanel.add(questionOnePanel);
		questionOnePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionOneLabel = new JLabel("Pain");
		questionOnePanel.add(questionOneLabel);
		
		painSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		painSlider.setPaintTicks(true);
		painSlider.setPaintLabels(true);
		painSlider.setMinimum(MINIMUM);
		painSlider.setMaximum(MAXIMUM);
		painSlider.setMajorTickSpacing(1);
		questionOnePanel.add(painSlider);
		
		JPanel questionTwoPanel = new JPanel();
		questionsPanel.add(questionTwoPanel);
		questionTwoPanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionTwoLabel = new JLabel("Swelling");
		questionTwoPanel.add(questionTwoLabel);
		
		swellingSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		questionTwoPanel.add(swellingSlider);
		swellingSlider.setPaintTicks(true);
		swellingSlider.setPaintLabels(true);
		swellingSlider.setMinimum(MINIMUM);
		swellingSlider.setMaximum(MAXIMUM);
		swellingSlider.setMajorTickSpacing(1);
		
		JPanel questionThreePanel = new JPanel();
		questionsPanel.add(questionThreePanel);
		questionThreePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionThreeLabel = new JLabel("Numbness");
		questionThreePanel.add(questionThreeLabel);
		
		numbnessSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		numbnessSlider.setPaintTicks(true);
		numbnessSlider.setPaintLabels(true);
		numbnessSlider.setMinimum(MINIMUM);
		numbnessSlider.setMaximum(MAXIMUM);
		numbnessSlider.setMajorTickSpacing(1);
		questionThreePanel.add(numbnessSlider);
		
		JPanel questionFourPanel = new JPanel();
		questionsPanel.add(questionFourPanel);
		
		JPanel submitPanel = new JPanel();
		questionsPanel.add(submitPanel);
		submitPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		submitLegIssueButton = new JButton("Submit Issue");
		submitLegIssueButton.addActionListener(new SubmitArmLegIssueButtonActionListener());
		
		outputLabel = new JLabel();
		outputLabel.setHorizontalAlignment(SwingConstants.CENTER);
		submitPanel.add(outputLabel);
		submitPanel.add(submitLegIssueButton);
	}
	
	private void loadHead() {
		frame.setTitle("Head");
		frame.setBounds(frame.getX(), frame.getY(), 567, 438);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel questionsPanel = new JPanel();
		frame.getContentPane().add(questionsPanel);
		questionsPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel instructionsPanel = new JPanel();
		questionsPanel.add(instructionsPanel);
		instructionsPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel instructionLabel = new JLabel("On a severity scale from 0 through 10, rate your expression of the following symptoms:");
		instructionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		instructionsPanel.add(instructionLabel);
		
		JPanel questionOnePanel = new JPanel();
		questionsPanel.add(questionOnePanel);
		questionOnePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionOneLabel = new JLabel("Headache");
		questionOnePanel.add(questionOneLabel);
		
		headacheSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		headacheSlider.setPaintTicks(true);
		headacheSlider.setPaintLabels(true);
		headacheSlider.setMinimum(MINIMUM);
		headacheSlider.setMaximum(MAXIMUM);
		headacheSlider.setMajorTickSpacing(1);
		questionOnePanel.add(headacheSlider);
		
		JPanel questionTwoPanel = new JPanel();
		questionsPanel.add(questionTwoPanel);
		questionTwoPanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionTwoLabel = new JLabel("Nausea");
		questionTwoPanel.add(questionTwoLabel);
		
		nauseaSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		questionTwoPanel.add(nauseaSlider);
		nauseaSlider.setPaintTicks(true);
		nauseaSlider.setPaintLabels(true);
		nauseaSlider.setMinimum(MINIMUM);
		nauseaSlider.setMaximum(MAXIMUM);
		nauseaSlider.setMajorTickSpacing(1);
		
		JPanel questionThreePanel = new JPanel();
		questionsPanel.add(questionThreePanel);
		questionThreePanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel questionThreeLabel = new JLabel("Shortness of Breath");
		questionThreePanel.add(questionThreeLabel);
		
		breathSlider = new JSlider(MINIMUM, MAXIMUM, MINIMUM);
		breathSlider.setPaintTicks(true);
		breathSlider.setPaintLabels(true);
		breathSlider.setMinimum(MINIMUM);
		breathSlider.setMaximum(MAXIMUM);
		breathSlider.setMajorTickSpacing(1);
		questionThreePanel.add(breathSlider);
		
		JPanel questionFourPanel = new JPanel();
		questionsPanel.add(questionFourPanel);
		
		JPanel submitPanel = new JPanel();
		questionsPanel.add(submitPanel);
		submitPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		submitHeadIssueButton = new JButton("Submit Issue");
		submitHeadIssueButton.addActionListener(new SubmitHeadIssueButtonActionListener());
		
		outputLabel = new JLabel();
		outputLabel.setHorizontalAlignment(SwingConstants.CENTER);
		submitPanel.add(outputLabel);
		submitPanel.add(submitHeadIssueButton);
		
		//for(String str : listContents)
			//for(char c : str.toCharArray())
				//c = '\0';
	}
	
	
	
	
	
	private class HeadButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			headButtonPressed();
		}
		
		private void headButtonPressed() {
			clearFrame();
			loadHead();
		}
	}
	private class ChestButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			chestButtonPressed();
		}
		
		private void chestButtonPressed() {
			clearFrame();
			loadChest();
		}
	}
	private class StomachButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			stomachButtonPressed();
		}
		
		private void stomachButtonPressed() {
			clearFrame();
			loadStomach();
		}
	}
	private class ArmsButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			armsButtonPressed();
		}
		
		private void armsButtonPressed() {
			clearFrame();
			loadArms();
		}
	}
	private class LegsButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			legsButtonPressed();
		}
		
		private void legsButtonPressed() {
			clearFrame();
			loadLegs();
		}
	}
	
	private class SubmitChestIssueButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			submitButtonPressed();
		}
		
		private void submitButtonPressed() {
			int pressure = pressureSlider.getValue();
			int heartburn = heartburnSlider.getValue();
			int chestpain = chestpainSlider.getValue();
			
			outputLabel.setText("pressure = " + pressure + ", heartburn = " + heartburn + ", chest pain = " + chestpain);
			Issue issue = new Issue("Chest", new int[] {pressure, heartburn, chestpain}, matchedPatient.issues.size() + 1 );
			matchedPatient.addIssue(issue);
			matchedPatient.serializeObject(patientIndex);
			submitChestIssueButton.setEnabled(false);
			
			List<Patient> patients = Patient.getCurrentPatients();
			for(Patient p : patients) {
				p.output();
			}
		}
	}
	
	private class SubmitStomachIssueButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			submitButtonPressed();
		}
		
		private void submitButtonPressed() {
			int indigestion = indigestionSlider.getValue();
			int appetite = appetitelossSlider.getValue();
			int constipation = constipationSlider.getValue();
			
			outputLabel.setText("indigestion = " + indigestion + ", loss of appetite = " + appetite + ", constipation = " + constipation);
			Issue issue = new Issue("Stomach", new int[] {indigestion, appetite, constipation}, matchedPatient.issues.size() + 1);
			matchedPatient.addIssue(issue);
			matchedPatient.serializeObject(patientIndex);
			submitStomachIssueButton.setEnabled(false);
			
			List<Patient> patients = Patient.getCurrentPatients();
			for(Patient p : patients) {
				p.output();
			}
		}
	}
	
	// arm and legs both use the same set of symptoms
	private class SubmitArmLegIssueButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			submitButtonPressed();
		}
		
		private void submitButtonPressed() {
			int pain = painSlider.getValue();
			int swelling = swellingSlider.getValue();
			int numbness = numbnessSlider.getValue();
			String armOrLeg = "";
			
			// check if the issue was an arm issue or leg issue
			if(submitArmIssueButton == null)
				armOrLeg = "Leg";
			else
				armOrLeg = "Arm";
			
			outputLabel.setText("pain = " + pain + ", swelling = " + swelling + ", numbness = " + numbness);
			Issue issue = new Issue(armOrLeg, new int[] {pain, swelling, numbness}, matchedPatient.issues.size() + 1);
			matchedPatient.addIssue(issue);
			matchedPatient.serializeObject(patientIndex);
			
			if(submitArmIssueButton == null)
				submitLegIssueButton.setEnabled(false);
			else
				submitArmIssueButton.setEnabled(false);
			
			List<Patient> patients = Patient.getCurrentPatients();
			for(Patient p : patients) {
				p.output();
			}
		}
	}
	
	private class SubmitHeadIssueButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			submitButtonPressed();
		}
		
		private void submitButtonPressed() {
			int headache = headacheSlider.getValue();
			int nausea = nauseaSlider.getValue();
			int breath = breathSlider.getValue();
			
			outputLabel.setText("headache = " + headache + ", nausea = " + nausea + ", shortness of breath = " + breath);
			Issue issue = new Issue("Head", new int[] {headache, nausea, breath}, matchedPatient.issues.size() + 1);
			matchedPatient.addIssue(issue);
			matchedPatient.serializeObject(patientIndex);
			submitHeadIssueButton.setEnabled(false);
			
			List<Patient> patients = Patient.getCurrentPatients();
			for(Patient p : patients) {
				p.output();
			}
		}
	}

}
