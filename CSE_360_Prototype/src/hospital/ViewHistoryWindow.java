package hospital;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JList;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewHistoryWindow {

	protected JFrame frame;
	private ViewPrescriptionWindow vPerWindow;
	/**
	 * Create the application.
	 */
	public ViewHistoryWindow(Patient patient) {
		loadHistoryGUI(patient);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void loadHistoryGUI(Patient patient) {
		frame = new JFrame();
		//frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		JPanel scrollListContents = fillList(patient);
		JScrollPane historyList = new JScrollPane();
		historyList.setViewportView(scrollListContents);
		frame.getContentPane().add(historyList);
		fillList(patient);
		frame.getContentPane().validate();
	}
	
	private JPanel fillList(Patient patient) {
		//DefaultListModel<JPanel> model = new DefaultListModel<JPanel>();
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(200, patient.issues.size() * 30));
		if(patient.issues != null)
			for(Issue issue : patient.issues) {
				String historyData1 = new String("");
				String historyData2 = new String("");
				String historyData3 = new String("");
				historyData1 = issue.getBodyPart() + "\n" + issue.getSymptom(0) + " " + issue.getSymptom(1) + " " + issue.getSymptom(2);
				historyData2 = "\n" + issue.getMonth() + "/" + issue.getDayOfMonth() + "/" + issue.getYear();
				historyData3 = "Status: " + getStatus(issue.getStatus());
				
				panel.setLayout(new GridLayout(patient.issues.size(),4,0,0));
				//model.addElement(panel);
				panel.add(new JLabel(historyData1));
				panel.add(new JLabel(historyData2));
				panel.add(new JLabel(historyData3));
				JButton darientGotBackButton = new JButton("View Prescription");
				darientGotBackButton.addActionListener(new DarientGotBackActionListener(issue));
				panel.add(darientGotBackButton);
				if(issue.getPrescription().equals("")) {
					darientGotBackButton.setVisible(false);
				}
				
				//add button
				//add action listener to button with or without lambda.
			}
		return panel;
	}
	private String getStatus(boolean open) {
		if(open)
			return "open";
		else
			return "closed";
	}
	
	class PanelRenderer implements ListCellRenderer {

        	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            JPanel renderedPanel = (JPanel) value;
            return renderedPanel;
        }
	}
	
	private class DarientGotBackActionListener implements ActionListener {
		Issue theIssue = null;
		
		DarientGotBackActionListener(Issue issue) {
			theIssue = issue;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(vPerWindow == null) {
				vPerWindow = new ViewPrescriptionWindow(theIssue);
				vPerWindow.frame.setVisible(true);
				vPerWindow.frame.setTitle("Prescription Window");
			}
			else {
				vPerWindow.frame.dispose();
				vPerWindow.frame = null;
				vPerWindow = null;
				vPerWindow = new ViewPrescriptionWindow(theIssue);
				vPerWindow.frame.setVisible(true);
				vPerWindow.frame.setTitle("Prescription Window");
			}
		}
		
	}

}
