package hospital;

import junit.framework.TestCase;
import java.util.Calendar;

public class IssueTest extends TestCase {
	public void testIssue() {
		String head = "head";
		int [] symptoms = {2, 2, 2};
		int issueNumber = 1;
		
		Issue test = new Issue(head, symptoms, issueNumber);
		assertEquals(true, (test instanceof Issue));

		String lolz = "";
		int [] symptoms1 = {0, 0, 0};
		issueNumber = 0;
	
		Issue test2 = new Issue(lolz, symptoms1, issueNumber);
		assertEquals(true, (test2 instanceof Issue));
	}
	
	public void testCalendar(){
		String head = "head";
		int [] symptoms = {2, 2, 2};
		int issueNumber = 1;
		
		Issue test = new Issue(head, symptoms, issueNumber);

		Calendar calendar = Calendar.getInstance();
		calendar.getTime();//fills the calendar object with data.
		int[] toDate = new int[5];
		toDate[0] = calendar.get(Calendar.YEAR);
		toDate[1] = calendar.get(Calendar.MONTH) + 1;
		toDate[2] = calendar.get(Calendar.DAY_OF_MONTH);
		toDate[3] = calendar.get(Calendar.DAY_OF_WEEK);
		toDate[4] = calendar.get(Calendar.HOUR_OF_DAY);
		
		assertEquals(toDate[0], test.getYear());
		assertEquals(toDate[1], test.getMonth());
		assertEquals(toDate[2], test.getDayOfMonth());
		assertEquals(toDate[3], test.getDayOfWeek());
		assertEquals(toDate[4], test.getHourOfDay());
	}

	public void testStatus(){
		String head = "head";
		int [] symptoms = {2, 2, 2};
		int issueNumber = 1;
		
		Issue test = new Issue(head, symptoms, issueNumber);
		assertEquals(true, test.getStatus());
		test.setStatus(false);
		assertEquals(false, test.getStatus());

		test.toggleStatus();
		assertEquals(true, test.getStatus());
	}
	
	public void testUrgency(){
		String head = "head";
		int [] symptoms = {2, 2, 2};
		int issueNumber = 1;
		
		Issue test = new Issue(head, symptoms, issueNumber);
		
		assertEquals(2, test.getUrgency());

		String lolz = "";
		int [] symptoms1 = {0, 0, 0};
		issueNumber = 0;
	
		Issue test2 = new Issue(lolz, symptoms1, issueNumber);

		assertEquals(0, test2.getUrgency());
		int [] maxSymptoms = {715827882, 715827882, 715827882};
		int [] minSymptoms = {-715827882, -715827882, -715827882};
		
		Issue test3 = new Issue(lolz, maxSymptoms, 5);
		assertEquals(715827882, test3.getUrgency());

		Issue test4 = new Issue(lolz, minSymptoms, 8);
		assertEquals(-715827882, test4.getUrgency());

	}

	public void testPrescription(){
		String head = "head";
		int [] symptoms = {2, 2, 2};
		int issueNumber = 1;
		
		Issue test = new Issue(head, symptoms, issueNumber);

		assertEquals("", test.getPrescription());

		test.setPrescription("Stop banging your head against the wall");
		assertEquals("Stop banging your head against the wall", 
			test.getPrescription());
	}
	
	public void testCompare(){
		String head = "head";
		int [] symptoms = {2, 2, 2};
		int issueNumber = 1;
		
		Issue test = new Issue(head, symptoms, issueNumber);
		
		int [] symptoms1 = {3, 3, 3};

		Issue test1 = new Issue(head, symptoms1, ++issueNumber);
		
		assertEquals(-1, test.compare(test, test1));
		assertEquals(1, test.compare(test1, test));
	}
}
