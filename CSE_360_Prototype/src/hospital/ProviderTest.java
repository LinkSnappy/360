package hospital;

import junit.framework.TestCase;

public class ProviderTest extends TestCase {

	public void testConstructor(){
		String fields[] = {"Will Graham", "123456789", "Street St.", 
			"1234567899"};
		int ID = 38;

		Provider test = new Provider(fields, ID);
	
		assertEquals(true, (test instanceof Provider)); 
	}

	public void testSettersAndGetters(){
		String fields[] = {"Will Graham", "123456789", "Street St.", 
			"1234567899"};
		int ID = 38;

		Provider test = new Provider(fields, ID);
		
		assertEquals("Will Graham", test.getName());
		assertEquals("123456789", test.getSSN());
		assertEquals("Street St.", test.getAddress());
		assertEquals("1234567899", test.getPhone());
		assertEquals(38, test.getID());

		test.setName("Nate");
		test.setSSN("987654321");
		test.setAddress("Nate St.");
		test.setPhone("9876543211");
		test.setID(2);

		assertEquals("Nate", test.getName());
		assertEquals("987654321", test.getSSN());
		assertEquals("Nate St.", test.getAddress());
		assertEquals("9876543211", test.getPhone());
		assertEquals(2, test.getID());
	}

	public void testCompare(){
		String fields[] = {"Will Graham", "123456789", "Street St.", 
			"1234567899"};
		int ID = 38;

		Provider test = new Provider(fields, ID);
		
		String fields1[] = {"Nate", "987654321", "Nate St.", 
			"9876543211"};
		ID = 2;

		Provider test1 = new Provider(fields1, ID);
		ProviderComparator comparator = new ProviderComparator();
	
		assertEquals(1, comparator.compare(test, test1));
		assertEquals(-1, comparator.compare(test1, test));
	}
}
