package hospital;

import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

@SuppressWarnings("rawtypes")
public class Patient extends User implements Databaser, java.io.Serializable, Comparable<Patient> {
	
	private int primaryProviderID; //the ID of the primary provider.
	private static final long serialVersionUID = 1L;
	protected ArrayList<Issue> issues;
	
	public Patient(String[] fields, int toID, int ppID) {
		setName(fields[0]);
		setSSN(fields[1]);
		setAddress(fields[2]);
		setPhone(fields[3]);
		setID(toID);
		setPrimaryProviderID(ppID);
		issues = new ArrayList<Issue>();
	}
	
	/* (non-Javadoc)
	 * @see prototype_unique_name.User#getName()
	 */
	@Override
	protected String getName() {
		// TODO Auto-generated method stub
		return super.getName();
		
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#setName(java.lang.String)
	 */
	@Override
	protected void setName(String name) {
		// TODO Auto-generated method stub
		super.setName(name);
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#getID()
	 */
	@Override
	protected int getID() {
		// TODO Auto-generated method stub
		return super.getID();
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#setID(java.lang.String)
	 */
	@Override
	protected void setID(int toID) {
		// TODO Auto-generated method stub
		super.setID(toID);
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#getSSN()
	 */
	@Override
	protected String getSSN() {
		// TODO Auto-generated method stub
		return super.getSSN();
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#setSSN(java.lang.String)
	 */
	@Override
	protected void setSSN(String sSN) {
		// TODO Auto-generated method stub
		super.setSSN(sSN);
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#getAccountNumber()
	 */
	@Override
	protected String getAccountNumber() {
		// TODO Auto-generated method stub
		return super.getAccountNumber();
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#setAccountNumber(java.lang.String)
	 */
	@Override
	protected void setAccountNumber(String accountNumber) {
		// TODO Auto-generated method stub
		super.setAccountNumber(accountNumber);
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#getAddress()
	 */
	@Override
	protected String getAddress() {
		// TODO Auto-generated method stub
		return super.getAddress();
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#setAddress(java.lang.String)
	 */
	@Override
	protected void setAddress(String address) {
		// TODO Auto-generated method stub
		super.setAddress(address);
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#getPhone()
	 */
	@Override
	protected String getPhone() {
		// TODO Auto-generated method stub
		return super.getPhone();
	}

	/* (non-Javadoc)
	 * @see prototype_unique_name.User#setPhone(java.lang.String)
	 */
	@Override
	protected void setPhone(String toPhone) {
		// TODO Auto-generated method stub
		super.setPhone(toPhone);
	}
	
	protected int getPrimaryProviderID() {
		return primaryProviderID;
	}
	
	protected void setPrimaryProviderID(int toPPID) {
		primaryProviderID = toPPID;
	}

	/**
	 * adds the newly created to the arraylist of issues.
	 * @param issue the new issue to be added.
	 */
	protected void addIssue(Issue issue) { 
		if(issues != null)
			issues.add(0, issue);
		else {
			issues = new ArrayList<Issue>();
			issues.add(0, issue);
		}
	}
	/**
	 * static method used to get the patient list so that a other classes can change or add data in the patient file. 
	 * @return returns a list of the files stored in the patient.ser file.
	 */
	protected static ArrayList<Patient> getCurrentPatients() {
		ArrayList<Patient> patients = null;
		FileInputStream fileIn = null;
		try {
			fileIn = new FileInputStream("patient.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			patients = (ArrayList<Patient>) in.readObject();
			in.close();
	        fileIn.close();
			
		}catch(EOFException  | FileNotFoundException e) {
	    	  return null;
		}catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return patients;
	}
	
	/**
	 * writes out the object that calls this method.
	 * @param indexToRemove This index is removed from the ArrayList<Patient> before it is serialized.  If you are making a change to the object pass in the index of the object.
	 */
	@Override
	public boolean serializeObject(int indexToRemove) {
		// TODO Auto-generated method stub
		boolean success = false;
		Patient patient = this;
		ArrayList<Patient> patients = null;
		
		patients = deserializeObjects();
		/*if(this != null && this.issues != null)
			this.issues = InsertionSortIssue(this.issues);*/
		if(indexToRemove != -1 && patients != null){
			try {
				patients.remove(indexToRemove);
			} catch(IndexOutOfBoundsException ex) {
				outputError(new File("error.txt"));
			}
		}
		if(patients == null)//will only occur if the file does not exist or has been made unreadable in some way.
				patients = new ArrayList<Patient>(); //instantiates a new ArrayList if deserializeObjects returns null (will only happen if the target file is file not found or empty.
		
		patients.add(patient);

		if(patients.size() > 1)
			Collections.sort(patients, new PatientComparator()); // uses
		try
	      {
			if(!(new File("patient.ser").exists())) 
				 new File("patient.ser").createNewFile();
		         FileOutputStream fileOut =
		         new FileOutputStream("patient.ser");//boolean arguement is whether or not the outputwriter should append.
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(patients);
		         out.close();
		         fileOut.close();
		        // System.out.printf("Serialized data is saved in \"patient.ser\"\n");
		         success = true;
	      }catch(IOException e)
	      {
	    	  success = false; //problem was caused and serialization 
	          e.printStackTrace();
	      }
		return success;
	}
	
	protected void serializeCollection(ArrayList<Patient> patientToSer) {//used for testing, delete.
		File patient = new File("patient.ser");
		if(!patient.exists())
			try {
				patient.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		try {
		         FileOutputStream fileOut =
		         new FileOutputStream("patient.ser");//boolean arguement is whether or not the outputwriter should append.
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(patientToSer);
		         out.close();
		         fileOut.close();
		        // System.out.printf("Serialized data is saved in \"patient.ser\"\n");
	      }catch(IOException e)
	      {
	      }
			
	}
	
	private void outputError(File file) {
		// TODO Auto-generated method stub
		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(file));
			dos.writeUTF("Index error occured after patient.ser was read in.");
			dos.close();
		}catch(IOException ex) {
			
		}
	}

	/**
	 * is used to pull the objects from patient.ser so that they can be used.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Patient> deserializeObjects() {
		ArrayList<Patient> patients = null;
		FileInputStream fileIn = null;
		try {
			fileIn = new FileInputStream("patient.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			
			patients = (ArrayList<Patient>) in.readObject();
			in.close();
	        fileIn.close();
			
		}catch(EOFException  | FileNotFoundException e) {
	    	  return null;
		}catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		return patients;
	}
	
	protected ArrayList<Issue> InsertionSortIssue(ArrayList<Issue> issuesList) {
		ArrayList<Issue> newIssuesList = new ArrayList<Issue>();
		Issue[] issues = new Issue[issuesList.size()];
		issuesList.toArray(issues);
		//Issue[] order = new Issue[issuesList.size()];
		Issue key = null;
		int j;              // the item to be inserted
	    int i;  
		
	    for (j = 1; j < issues.length; j++) {
	    	key = issues[j];
	    	for(i = (j-1); (i >= 0) && (issues[i].getUrgency() < key.getUrgency()); i--) {
	    		issues[i+1] = issues[i];
	    	}
	    	issues[i+1] = key;
	    }
	    
	    for(Issue issue : issues) {
	    	newIssuesList.add(issue);
	    }
	    return newIssuesList;
	}
	
	@Override
	public void output(){
	      System.out.println("\nName: " + getName());
	      System.out.println("ID = " + getID());
	      System.out.println("Address: " + getAddress());
	      System.out.println("SSN: " + getSSN());
	      System.out.println("Number: " + getPhone());
	      System.out.println("Primary provider ID = " + primaryProviderID);
	      if(issues == null)
	    	  System.out.println("issues is null");
	      if(issues != null)
	    	  for(Issue is : issues) {
	    		  System.out.println("body part = " + is.getBodyPart());
	    		  for(int i = 0; i < is.getSymptom().length; ++i){
	    			  System.out.println("symptom " + Integer.toString(i + 1) + " = " + is.getSymptom(i));
	    		  }
	    		  System.out.println("Urgency: " + is.getUrgency() + "\n");
	    	  }
	}
	
	@Override
	public Patient getPatientData() {
		return null;
		// TODO Auto-generated method stub
		
	}

	@Override
	public int compareTo(Patient o) {
		// TODO Auto-generated method stub
		return 0;
	}

}

/**
 * class used to sort a user created object.
 *
 */
class PatientComparator implements Comparator<Patient> {

	@Override
	public int compare(Patient p1, Patient p2) {
		// TODO Auto-generated method stub
		if(p1.getID() > p2.getID())
			return 1;
		else 
			return -1;
	}
	
}

//For serialization encrypted:
/*SealedObject sealedObject = null;
Cipher cipher = null;
try {
	cipher = Cipher.getInstance("DES");
	cipher.init(Cipher.ENCRYPT_MODE, key);
	
	sealedObject = new SealedObject(patient, cipher);
} catch (IllegalBlockSizeException | NoSuchAlgorithmException
		| NoSuchPaddingException | IOException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
} catch (InvalidKeyException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}*/

//For serialization encrypted:
/*SealedObject sealedObject = null;
Cipher cipher = null;

try
  {
     FileInputStream fileIn = new FileInputStream("C:/Users/1x2kb/workspace/CSE_360_Prototype/Output/patient.ser
     ObjectInputStream in = new ObjectInputStream(fileIn);
     sealedObject = (SealedObject) in.readObject();
     in.close();
     fileIn.close();
     
     System.out.println(sealedObject.getClass());
     System.out.println(sealedObject.getAlgorithm());
     
     cipher = Cipher.getInstance("DES");
     cipher.init(Cipher.DECRYPT_MODE, key);
     patient = (Patient) sealedObject.getObject(cipher);
     success = true;
  }catch(IOException e)
  {
     e.printStackTrace();
     success = false;
  }catch(ClassNotFoundException c)
  {
     System.out.println("Patient class not found");
     c.printStackTrace();
     success = false;
  } catch (IllegalBlockSizeException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (BadPaddingException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (NoSuchAlgorithmException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (NoSuchPaddingException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (InvalidKeyException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}*/
